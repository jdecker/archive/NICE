"""
Startup script for the NICE node. It starts
    - an IPFS node,
    - a P2P node (consisting of a tcp server endpoint and multiple client endpoints),
    - and a NICE Blockchain
via a command line interface built with Click. The CLI provides some configurability
for the NICE node such as specifying interfaces/ports for the IPFS or P2P node.

:author: Jero Schäfer
:last_modified: 03.07.2020
"""

import logging
import subprocess
import time
import threading

from flask import Flask
from flask_bootstrap import Bootstrap
from flask_uploads import configure_uploads
from requests import get
from twisted.internet import task
from twisted.internet import reactor
from twisted.internet.endpoints import TCP4ServerEndpoint

import block_logger
from basicblockchain.Blockchain import Blockchain
from config import *
from p2p.p2p_server import P2PProtocolFactory
from webfrontend.forms import upload_set_audio
from webfrontend.frontend import frontend, nav, frontend_manager

# setup logger
block_logger.setup_logger(console_level=logging.DEBUG)
logger = logging.getLogger("block_logger")


def start(p2p_host, p2p_port, p2p_node_list, p2p_bootstrap, ipfs_enabled, flask_enabled, flask_host, flask_port,
          chain_path, queue_path, chain_id):
    """
    Start the NICE node with the given configuration
    :param p2p_host: P2P server host
    :type p2p_host: str (default "127.0.0.1")
    :param p2p_port: P2P server port
    :type p2p_port: int (default 5005)
    :param p2p_node_list: File with P2P node list
    :type p2p_node_list: str or None
    :param p2p_bootstrap: P2P bootstrap node list
    :type p2p_bootstrap: list of (host,port)
    :param ipfs_enabled: Flag to enable/disable IPFS
    :type ipfs_enabled: bool (default True)
    :param flask_enabled: Flag to enable/disable Flask
    :type flask_enabled: bool (default True)
    :param flask_host: Flask host (if None
    :type flask_host: str (defailt "127.0.0.1")
    :param flask_port: Flask port
    :type flask_host: int (default 5000)
    :param chain_path: Path to the blockchain file
    :type chain_path: str
    :param queue_path: Path to the transaction queue file (relative to this file or absolute)
    :type queue_path: str
    :param chain_id: ID of Blockchain or None
    :type chain_id: str or None
    :return: None
    """
    logger.debug(f"Start NICE: Interface={p2p_host}, port={p2p_port}, p2p_bootstrap={p2p_bootstrap}, "
                 f"ipfs_enabled={ipfs_enabled}, flask_enabled={flask_enabled}, chain_id={chain_id}")

    # Start ipfs daemon
    if ipfs_enabled:
        ipfs_protocol = start_ipfs()

    # Setup flask (has frontend manager with blockchain)
    app = setup_flask(chain_path, queue_path, chain_id=chain_id)
    blockchain = app.frontend_manager.blockchain
    if chain_id is None:
        logger.info(f"NICE Blockchain ID: {blockchain.get_chain_identifier()}")

    # Start the P2P node and discover new peers
    p2p_node = start_p2p(p2p_host, p2p_port, blockchain, chain_id, p2p_node_list)
    app.frontend_manager.init_p2p_node(p2p_node)

    # If joining an existing NICE blockchain, discover other peers and request the chain
    # Say hello with NodeID message to new discovered peers
    if p2p_node.node_list.is_empty():
        deferred = p2p_node.discover_peers(p2p_bootstrap)
    else:
        deferred = p2p_node.discover_peers([])  # empty list, no bootstrapping required

    #if chain_id is not None:
    # Request the chain from or update it with one of the discovered peers
    deferred_list = task.deferLater(reactor, 4, p2p_node.update_chain, p2p_bootstrap)
    deferred.chainDeferred(deferred_list)

    # Start Flask web frontend (if not disabled)
    if flask_enabled:
        reactor.callInThread(app.run, host=flask_host, port=flask_port)

    # Fix to the timer not starting error by using threading events to get the timer start to the reactor
    event = threading.Event()
    loop = task.LoopingCall(checkEvent, event, app)
    loop.start(30)
    app.frontend_manager.add_event(event)
    p2p_node.add_event(event)
    if blockchain.get_queue_size() > 0:
        # Pending transactions were loaded from queue file, start block timer
        frontend_manager.trigger_transaction_check()

    # Run the twisted reactor
    # reactor.callLater(10, reactor.stop)
    reactor.run()

    # Shutdown everything, Flask automatically stops
    if flask_enabled:
        app.ipfs_node.close()
        app.logger.debug("IPFS client closed")
    if ipfs_enabled:
        # ipfs_protocol.shutdown()
        try:
            ipfs_protocol.terminate()
            ipfs_protocol.wait(timeout=5)
        except subprocess.TimeoutExpired as e:
            ipfs_protocol.kill()
    if p2p_node:
        print(p2p_node.node_list.to_string())
        print(p2p_node.chain)
        p2p_node.stopFactory()


def checkEvent(event, app):
    """
    In order to call check transaction queue from the reactor thread this function is needed.
    If the event is set, check transaction queue is called.
    :param event: The threading event set in by the flask sever upon adding a transaction.
    :type event: Threading Event
    :param app: The flask app, to call check transaction queue
    :type app: Flask App
    :return: No return
    """
    logger.debug(f"Checking event, its {event.is_set()}")
    if event.is_set():
        event.clear()
        app.frontend_manager.check_transaction_queue()


def start_p2p(interface, port, blockchain, chain_id, node_list_path):
    """
    Start the P2P node (as server)
    :param interface: P2P server interface
    :type interface: str
    :param port: P2P server port
    :type port: int
    :param blockchain: A NICE blockchain (can be empty (join) or with genesis block (new))
    :type blockchain: Blockchain
    :param chain_id: ID of an existing NICE blockchain or None if a new chain was created
    :type chain_id: str or None
    :param node_list_path: File path with node list
    :type node_list_path: str or None
    :return: P2P node
    :rtype: P2PProtocolFactory
    """

    # Start tcp server at the given interface and port
    if interface:
        endpoint = TCP4ServerEndpoint(reactor, port, interface=interface)
    else:
        endpoint = TCP4ServerEndpoint(reactor, port)
        interface = get_external_ip()

    # Start Factory with empty chain and chain_id or with a new chain
    p2p_node = P2PProtocolFactory(interface, port,
                                  blockchain=blockchain, chain_id=chain_id, node_list_path=node_list_path)
    endpoint.listen(p2p_node)
    logger.info(f"Started P2P Node (node id={p2p_node.node_id})")
    return p2p_node


def get_external_ip():
    return get('https://api.ipify.org').text


def setup_flask(chain_path, queue_path, chain_id=None):
    """
    Setup flask app and do all the configuration needed.
    Return the app object to start the webserver
    :param chain_path: Path to the blockchain file
    :type chain_path: str
    :param queue_path: Path to the transaction queue file (relative to this file or absolute)
    :type queue_path: str
    :param chain_id: Chain ID of the NICE blockchain to join to or None
                     if a new chain should be started
    :type chain_id: str or None
    :return: Flask application object
    :rtype: Flask
    """
    app = Flask(__name__)

    app.logger = logger

    # Initialize Bootstrap framework
    bootstrap = Bootstrap(app)
    logger.debug("Flask: Initialized Bootstrap framework")

    # Register Blueprint for the frontend routes
    app.register_blueprint(frontend)
    logger.debug("Flask: Registered Blueprints")

    # Initialize Flask NavigationBar Extension
    nav.init_app(app)
    logger.debug("Flask: Initialized NavigationBar Extension")

    # Initialize Secret Key
    app.config["SECRET_KEY"] = "any secret string"

    # Flask-Uploads extension
    app.config['UPLOADS_DEFAULT_DEST'] = "webfrontend/static"
    app.config['MAX_CONTENT_LENGTH'] = 20 * 1024 * 1024  # limit uploads to 20 MB
    configure_uploads(app, upload_set_audio)
    logger.debug(f"Configured upload set {upload_set_audio.name}")

    # Get frontend manager
    app.frontend_manager = frontend_manager
    app.frontend_manager.init_blockchain(chain_path, queue_path, chain_id=chain_id)
    app.frontend_manager.ipfs_node.connect_to_network()
    logger.debug(f"Setup chain, length={app.frontend_manager.blockchain.get_length()}")

    return app


def start_ipfs():
    """
    Start an IPFS daemon via a subprocess.
    :return: The process handle of the IPFS daemon
    :rtype: Popen
    """
    ipfs_process = subprocess.Popen(["ipfs", "daemon"])
    time.sleep(3)
    return ipfs_process


if __name__ == '__main__':
    start(P2P_HOST, P2P_PORT, P2P_NODES_FILE, P2P_BOOTSTRAP,
          IPFS_ENABLED, FLASK_ENABLED, FLASK_HOST, FLASK_PORT,
          BLOCKCHAIN_FILE, QUEUE_FILE, BLOCKCHAIN_ID)
