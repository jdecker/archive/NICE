# NICE Style Guide

# Code

The code should follow the best practices:

DRY: Do not repeat yourself

KISS: Keep it simple stupid

and so on.

## Formatting

Code should be formatted according to the PEP 8 guidelines for python code formatting.
This is usually enforceable by the IDE.

## Naming

Variable names should be expressive,
variable, file and function names should use snake case,
for example:

block_logger.py
setup_logger()
path_to_logs

When applicable the variable name should also contain a hint on the type of the variable:

path_to_logs_str = str(path_to_logs)

Custom classes should be capitalized and use camel case:

class BlockChain():

# Documentation

Docstrings should be of the reStructuredText format. 

## File
Every new file should start with a documentation block of the form:

"""
Description of what the purpose of this file is and what it does.
If applicable note the function that should usually be called when importing this file.

TODO: Anything that still needs to be done here

:author: The main person responsible for this code.
:last_modified: When the file was last changed as date without exact time.
"""

## Function

Every function should have a docstring of the form:

def function(var1):
"""
Description of the main purpose of the function, how to use it, what it returns.
:param var1: Description of the first input parameter and if applicable standard values.
:type var1: type of the parameter, for example simply 'str' for a string or List[str] for a list of strings
:return: Description of the returned value, or of what has changed in case the return is None
:rtype: type of the return value or None if nothing is returned
"""

It is also possible to add type hints directly to the function definition:

def function(var1: str = "logger") -> None:

This can be done but isn't required.

## Inline

Comments should help the reader understand the code.
Comments should explain why a block of code does something a certain way.
If a line of code is complex and has to be complex, a comment should explain what the code does.
If applicable, comments should structure a function into phases.

Comments should be above the line(s) they describe and not in the same line as code.

# Testing

PyTest should be used to perform automated testing before every push to master.

## General testing

Every non trivial function should have a respective test function which can be automatically executed by PyTest.

When testing the file 
block_logger.py,
the test file should be called 
test_block_logger.py

The test functions inside test_block_logger.py should all start with test_
The test functions don't need to be documented the same way as normal functions, still it should be noted what is tested.
The file also only needs to note the author and when it was last modified.

It is usually better to use one test function for each test case and to name the functions accordingly.

## Assert preconditions

Functions that are expected to be imported into other files or be called from outside the file they are declared in, should assert their input parameter.

In order to avoid unexpected behaviour due to incorrect input parameters being used, the function should assert the input parameters and sometimes returnvalues from function calls to throw an error instead.

For example:

def delete_oldest_log(path_to_logs):
"""
:description: Finds and deletes the oldest .log file in the given file path
:param path_to_logs: file path to the log folder
:type path_to_logs: pathlib.PATH
:return: None, but the oldest log was deleted
:rtype: None
"""

assert path_to_logs.exists()
assert path_to_logs.is_dir()
assert len(list(path_to_logs.glob('*.log'))) > 0

## Fixes

When fixing a non trivial bug, a short description (and reference if applicable) of the bug should be added as a comment to the code fixing the bug.

Further if applicable a test and/or assert should be added to detect whether the bug occurs again.

# Logging

For logging the block_logger.py offers a configured logger.
For directly using the logger it needs to be imported as

import logging

logger = logging.getLogger("block_logger")

Further the flask app logger is also set as the same logger so

app.logger

is the same.

Possible logging levels are 
Critical, Error, Warning, Info, Debug

Don't hold back with Debug level logs as we can simply filter them out later.
