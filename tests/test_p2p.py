"""
Tests for the p2p network and consensus algorithm.

Either run the tests one by one or install pytest-xdist
pip install pytest-xdist

and run them all in parallel in a terminal
pytest test_p2p.py -n <number of tests>

The number must be the number of tests or it will try to reuse a reactor and fail.

:author: Jonathan Decker
:last_modified: 25.6.2020
"""

import block_logger
import logging
from twisted.internet import defer, reactor
from twisted.internet.endpoints import TCP4ServerEndpoint, TCP4ClientEndpoint, connectProtocol
from twisted.internet import error as twisted_error
from twisted.python.failure import Failure
from p2p.p2p_server import P2PProtocolFactory, State, P2PProtocol, cb_client_action
from basicblockchain import Blockchain, Block, Transaction
from basicblockchain.Sign import create_normal_signature
import time
import random


block_logger.setup_logger(console_level=logging.DEBUG, create_log_files=False)
logger = logging.getLogger("block_logger")

"""
TODO
- Test for invalid Block
    - invalid hash
    - invalid ID
    - invalid number of transactions
    - Other object beside transaction in Block
- Test for invalid transaction (all variants)
    - invalid signature
        - wrong hash
        - signature is not valid
"""


def p2p_connection_refused_errback(failure):
    """
    Error callback
    :param failure:
    :type failure: Failure
    :return:
    """
    failure.trap(twisted_error.ConnectionRefusedError)  # handle ConnectionRefusedError by skipping that peer
    logger.debug(f"P2P Connection refused, ignoring unavailable peer")


class TestEnvironment:
    """
    Generic test environment for testing the p2p network.
    """

    nodes = []

    def create_node(self, interface, port, blockchain, node_list, chain_id=None):
        """

        :param chain_id:
        :param interface:
        :param port:
        :param blockchain:
        :param node_list:
        :return:
        """
        node = self.Node(interface, port, blockchain, node_list, chain_id)
        self.nodes.append(node)
        return node

    def start_reactor(self, stop_in=-1):
        # This monstrosity makes it possible to restart the reactor
        if stop_in > 0:
            reactor.callLater(stop_in, reactor.stop)
        reactor.run()

    class Node:
        """
        Representation of a single node with a p2p server and a Blockchain.
        """

        def __init__(self, interface, port, node_list, blockchain, chain_id=None):

            assert type(interface) == str
            assert type(port) == int
            assert type(node_list) == dict
            assert type(blockchain) == Blockchain.Blockchain

            self.blockchain = blockchain
            if chain_id is None:
                self.chain_id = self.blockchain.get_chain_identifier()
            else:
                self.chain_id = chain_id
            self.interface = interface
            self.port = port
            self.p2p_server = self.setup_p2p_server(node_list)
            self.node_id = self.p2p_server.node_id
            self.node_list = self.p2p_server.node_list
            self.deferred = defer.Deferred()

        def setup_p2p_server(self, node_list):
            """

            :return:
            """
            endpoint = TCP4ServerEndpoint(reactor, self.port, interface=self.interface)
            p2p_server = P2PProtocolFactory(self.interface, self.port, blockchain=self.blockchain,
                                            start_as_server=True, chain_id=self.chain_id,
                                            node_list=node_list)
            endpoint.listen(p2p_server)
            logger.debug(f"Started p2p server on {self.interface}:{self.port}")
            return p2p_server

        def full_update_node_list(self):
            for node_id, host, port in self.node_list.iterate():
                if host == self.interface and port == self.port:
                    continue
                self.update_node_list(host, port)

        def update_node_list(self, peer_host, peer_port):
            client = TCP4ClientEndpoint(reactor, peer_host, peer_port)
            d = connectProtocol(client, P2PProtocol(self.p2p_server, start_as_server=False, state=State.CLIENT_EXCHANGE_PEERS))
            d.addCallbacks(cb_client_action, p2p_connection_refused_errback)
            self.deferred.chainDeferred(d)

        def update_blockchains(self):
            for node_id, host, port in self.node_list.iterate():
                if host == self.interface and port == self.port:
                    continue
                self.update_blockchain(host, port)

        def update_blockchain(self, peer_host, peer_port):
            client = TCP4ClientEndpoint(reactor, peer_host, peer_port)
            d = connectProtocol(client, P2PProtocol(self.p2p_server, start_as_server=False, state=State.CLIENT_UPDATE_CHAIN))
            d.addCallbacks(cb_client_action, p2p_connection_refused_errback)
            self.deferred.chainDeferred(d)

        def request_blockchains(self):
            for node_id, host, port in self.node_list.iterate():
                if host == self.interface and port == self.port:
                    continue
                self.request_blockchain(host, port)
                # This should stop as soon as one preset address responds and it should only try one at a time

        def request_blockchain(self, peer_host, peer_port):
            client = TCP4ClientEndpoint(reactor, peer_host, peer_port)
            d = connectProtocol(client, P2PProtocol(self.p2p_server, start_as_server=False, state=State.CLIENT_NEW_NODE))
            d.addCallbacks(cb_client_action, p2p_connection_refused_errback)
            # cb_client_action(connectProtocol(client, P2PProtocol(self.p2p_server, start_as_server=False, state=State.CLIENT_NEW_NODE)))
            self.deferred.chainDeferred(d)


def create_dummy_transaction(pub_key, priv_key):
    time_stamp = time.time()
    ipfshash = "pseudohash"
    title = "Song " + str(random.randint(0, 1000))
    sign = create_normal_signature(ipfshash, pub_key, time_stamp, title, None, None, None, None, priv_key)
    transaction = Transaction.Transaction.create_transaction(ipfshash, pub_key, time_stamp, sign, title)
    return transaction


def create_dummy_blocks(number, chain, priv_key):
    pub_key = chain.get_chain_pubkey()

    for i in range(number):
        trans = []
        for j in range(5):
            trans.append(create_dummy_transaction(pub_key, priv_key))
        chain.add_transactions_to_queue(trans)
        Block.Block(chain)


def test_p2p_node_exchange():

    # create the test environment
    env = TestEnvironment()

    # setup the first node
    bc, priv_key = Blockchain.Blockchain.init_blockchain()
    node1 = env.create_node("127.0.0.1", 5027, dict(), bc)
    preset_node_list = {node1.node_id: {"host": node1.interface, "port": node1.port, "timestamp": time.time()}}

    # setup further nodes which only know the first node
    node2 = env.create_node("127.0.0.1", 5028, preset_node_list, bc)
    node3 = env.create_node("127.0.0.1", 5029, preset_node_list, bc)
    # node4 = env.create_node("127.0.0.1", 5030, preset_node_list, bc)

    # add tasks to further nodes to run update node list
    node2.full_update_node_list()
    node3.full_update_node_list()

    # let it run for a moment
    env.start_reactor(stop_in=5)

    # check if the node lists were updates are correct
    for node in env.nodes:
        print(node.node_list.to_string())

    # assert node1.node_list.to_string() == node2.node_list.to_string()
    assert node1.node_list.equals(node2.node_list)
    assert node2.node_list.equals(node1.node_list)
    assert node3.node_list.equals(node1.node_list)


def test_new_node():

    # create the test environment
    env = TestEnvironment()

    # setup the first node
    bc, priv_key = Blockchain.Blockchain.init_blockchain()
    pub_key = bc.get_chain_pubkey()
    node1 = env.create_node("127.0.0.1", 5005, dict(), bc)
    preset_node_list = {node1.node_id: {"host": node1.interface, "port": node1.port, "timestamp": time.time()}}

    # add some blocks to first nodes chain
    create_dummy_blocks(5, bc, priv_key)

    # setup a new node
    bc2 = Blockchain.Blockchain()
    node2 = env.create_node("127.0.0.1", 5006, preset_node_list, bc2, chain_id=bc.get_chain_identifier())

    # add tasks to new node
    node2.request_blockchains()

    # let it run for a moment
    env.start_reactor(5)

    # check if the blockchain is now correct
    print("\n FIRST CHAIN:\n")
    print(str(bc))
    print("\n SECOND CHAIN:\n")
    print(str(bc2))

    assert str(bc) == str(bc2)


def test_client_updates_server():

    # create test environment
    env = TestEnvironment()

    # setup the first node
    bc, priv_key = Blockchain.Blockchain.init_blockchain()
    node1 = env.create_node("127.0.0.1", 5007, dict(), bc)
    preset_node_list = {node1.node_id: {"host": node1.interface, "port": node1.port, "timestamp": time.time()}}

    # add some blocks to the chain
    create_dummy_blocks(3, bc, priv_key)

    # make a copy of the chain
    bc2 = Blockchain.Blockchain()
    blocks = bc.get_blocks_starting_at_ID(0)
    for block in blocks:
        bc2.append_block(block)

    assert str(bc) == str(bc2)

    # create second node
    node2 = env.create_node("127.0.0.1", 5008, preset_node_list, bc2, chain_id=bc2.get_chain_identifier())

    # let client create new blocks
    create_dummy_blocks(2, bc2, priv_key)

    assert str(bc) != str(bc2)

    # add task
    node2.update_blockchains()

    # let it run for a moment
    env.start_reactor(5)

    # check if the update worked
    print("\n FIRST CHAIN:\n")
    print(str(bc))
    print("\n SECOND CHAIN:\n")
    print(str(bc2))

    assert str(bc) == str(bc2)


def test_server_updates_client():

    # create test environment
    env = TestEnvironment()

    # setup the first node
    bc, priv_key = Blockchain.Blockchain.init_blockchain()
    node1 = env.create_node("127.0.0.1", 5009, dict(), bc)
    preset_node_list = {node1.node_id: {"host": node1.interface, "port": node1.port, "timestamp": time.time()}}

    # add some blocks to the chain
    create_dummy_blocks(3, bc, priv_key)

    # make a copy of the chain
    bc2 = Blockchain.Blockchain()
    blocks = bc.get_blocks_starting_at_ID(0)
    for block in blocks:
        bc2.append_block(block)

    assert str(bc) == str(bc2)

    # create second node
    node2 = env.create_node("127.0.0.1", 5010, preset_node_list, bc2, chain_id=bc2.get_chain_identifier())

    # let server create new blocks
    create_dummy_blocks(2, bc, priv_key)

    assert str(bc) != str(bc2)

    # add task
    node2.update_blockchains()

    # let it run for a moment
    env.start_reactor(5)

    # check if the update worked
    print("\n FIRST CHAIN:\n")
    print(str(bc))
    print("\n SECOND CHAIN:\n")
    print(str(bc2))

    assert str(bc) == str(bc2)

# mismatch tests


def test_mismatch_same_length_server_is_older():

    # create test environment
    env = TestEnvironment()

    # setup first node
    bc, priv_key = Blockchain.Blockchain.init_blockchain()
    pub_key = bc.get_chain_pubkey()
    node1 = env.create_node("127.0.0.1", 5011, dict(), bc)
    preset_node_list = {node1.node_id: {"host": node1.interface, "port": node1.port, "timestamp": time.time()}}

    # add some blocks to the chain
    create_dummy_blocks(3, bc, priv_key)

    # make a copy of the chain
    bc2 = Blockchain.Blockchain()
    blocks = bc.get_blocks_starting_at_ID(0)
    for block in blocks:
        bc2.append_block(block)

    assert str(bc) == str(bc2)

    # create second node
    node2 = env.create_node("127.0.0.1", 5012, preset_node_list, bc2, chain_id=bc2.get_chain_identifier())

    # let server first create a new block
    create_dummy_blocks(1, bc, priv_key)

    # make sure timestamps are different
    time.sleep(1)

    # let client create a new block afterwards
    create_dummy_blocks(1, bc2, priv_key)

    assert str(bc) != str(bc2)
    assert bc.get_length() == bc2.get_length()

    # add task
    node2.update_blockchains()

    # let it run for a moment
    env.start_reactor(5)

    # check if the update worked
    print("\n FIRST CHAIN:\n")
    print(str(bc))
    print("\n SECOND CHAIN:\n")
    print(str(bc2))

    assert str(bc) == str(bc2)


def test_mismatch_same_length_server_is_older_two_blocks():

    # create test environment
    env = TestEnvironment()

    # setup first node
    bc, priv_key = Blockchain.Blockchain.init_blockchain()
    node1 = env.create_node("127.0.0.1", 5013, dict(), bc)
    preset_node_list = {node1.node_id: {"host": node1.interface, "port": node1.port, "timestamp": time.time()}}

    # add some blocks to the chain
    create_dummy_blocks(3, bc, priv_key)

    # make a copy of the chain
    bc2 = Blockchain.Blockchain()
    blocks = bc.get_blocks_starting_at_ID(0)
    for block in blocks:
        bc2.append_block(block)

    assert str(bc) == str(bc2)

    # create second node
    node2 = env.create_node("127.0.0.1", 5014, preset_node_list, bc2, chain_id=bc2.get_chain_identifier())

    # let server first create two new blocks
    create_dummy_blocks(2, bc, priv_key)

    # make sure timestamps are different
    time.sleep(1)

    # let client create two new blocks afterwards
    create_dummy_blocks(2, bc2, priv_key)

    assert str(bc) != str(bc2)
    assert bc.get_length() == bc2.get_length()

    # add task
    node2.update_blockchains()

    # let it run for a moment
    env.start_reactor(5)

    # check if the update worked
    print("\n FIRST CHAIN:\n")
    print(str(bc))
    print("\n SECOND CHAIN:\n")
    print(str(bc2))

    assert str(bc) == str(bc2)


def test_mismatch_same_length_server_is_younger():
    # create test environment
    env = TestEnvironment()

    # setup first node
    bc, priv_key = Blockchain.Blockchain.init_blockchain()
    pub_key = bc.get_chain_pubkey()
    node1 = env.create_node("127.0.0.1", 5015, dict(), bc)
    preset_node_list = {node1.node_id: {"host": node1.interface, "port": node1.port, "timestamp": time.time()}}

    # add some blocks to the chain
    create_dummy_blocks(3, bc, priv_key)

    # make a copy of the chain
    bc2 = Blockchain.Blockchain()
    blocks = bc.get_blocks_starting_at_ID(0)
    for block in blocks:
        bc2.append_block(block)

    assert str(bc) == str(bc2)

    # create second node
    node2 = env.create_node("127.0.0.1", 5016, preset_node_list, bc2, chain_id=bc2.get_chain_identifier())

    # let client first create a new block
    create_dummy_blocks(1, bc2, priv_key)

    # make sure timestamps are different
    time.sleep(1)

    # let server create a new block afterwards
    create_dummy_blocks(1, bc, priv_key)

    assert str(bc) != str(bc2)
    assert bc.get_length() == bc2.get_length()

    # add task
    node2.update_blockchains()

    # let it run for a moment
    env.start_reactor(5)

    # check if the update worked
    print("\n FIRST CHAIN:\n")
    print(str(bc))
    print("\n SECOND CHAIN:\n")
    print(str(bc2))

    assert str(bc) == str(bc2)


def test_mismatch_same_length_server_is_younger_two_blocks():
    # create test environment
    env = TestEnvironment()

    # setup first node
    bc, priv_key = Blockchain.Blockchain.init_blockchain()
    pub_key = bc.get_chain_pubkey()
    node1 = env.create_node("127.0.0.1", 5017, dict(), bc)
    preset_node_list = {node1.node_id: {"host": node1.interface, "port": node1.port, "timestamp": time.time()}}

    # add some blocks to the chain
    create_dummy_blocks(3, bc, priv_key)

    # make a copy of the chain
    bc2 = Blockchain.Blockchain()
    blocks = bc.get_blocks_starting_at_ID(0)
    for block in blocks:
        bc2.append_block(block)

    assert str(bc) == str(bc2)

    # create second node
    node2 = env.create_node("127.0.0.1", 5018, preset_node_list, bc2, chain_id=bc2.get_chain_identifier())

    # let client first create a new block
    create_dummy_blocks(2, bc2, priv_key)

    # make sure timestamps are different
    time.sleep(1)

    # let server create a new block afterwards
    create_dummy_blocks(2, bc, priv_key)

    assert str(bc) != str(bc2)
    assert bc.get_length() == bc2.get_length()

    # add task
    node2.update_blockchains()

    # let it run for a moment
    env.start_reactor(5)

    # check if the update worked
    print("\n FIRST CHAIN:\n")
    print(str(bc))
    print("\n SECOND CHAIN:\n")
    print(str(bc2))

    assert str(bc) == str(bc2)


def test_mismatch_client_is_longer():
    # create test environment
    env = TestEnvironment()

    # setup first node
    bc, priv_key = Blockchain.Blockchain.init_blockchain()
    pub_key = bc.get_chain_pubkey()
    node1 = env.create_node("127.0.0.1", 5019, dict(), bc)
    preset_node_list = {node1.node_id: {"host": node1.interface, "port": node1.port, "timestamp": time.time()}}

    # add some blocks to the chain
    create_dummy_blocks(3, bc, priv_key)

    # make a copy of the chain
    bc2 = Blockchain.Blockchain()
    blocks = bc.get_blocks_starting_at_ID(0)
    for block in blocks:
        bc2.append_block(block)

    assert str(bc) == str(bc2)

    # create second node
    node2 = env.create_node("127.0.0.1", 5020, preset_node_list, bc2, chain_id=bc2.get_chain_identifier())

    # let server create a new block
    create_dummy_blocks(1, bc, priv_key)

    # let client create two new blocks
    create_dummy_blocks(2, bc2, priv_key)

    assert str(bc) != str(bc2)
    assert bc.get_length() < bc2.get_length()

    # add task
    node2.update_blockchains()

    # let it run for a moment
    env.start_reactor(5)

    # check if the update worked
    print("\n FIRST CHAIN:\n")
    print(str(bc))
    print("\n SECOND CHAIN:\n")
    print(str(bc2))

    assert str(bc) == str(bc2)


def test_mismatch_client_is_longer_two_blocks():
    # create test environment
    env = TestEnvironment()

    # setup first node
    bc, priv_key = Blockchain.Blockchain.init_blockchain()
    pub_key = bc.get_chain_pubkey()
    node1 = env.create_node("127.0.0.1", 5021, dict(), bc)
    preset_node_list = {node1.node_id: {"host": node1.interface, "port": node1.port, "timestamp": time.time()}}

    # add some blocks to the chain
    create_dummy_blocks(3, bc, priv_key)

    # make a copy of the chain
    bc2 = Blockchain.Blockchain()
    blocks = bc.get_blocks_starting_at_ID(0)
    for block in blocks:
        bc2.append_block(block)

    assert str(bc) == str(bc2)

    # create second node
    node2 = env.create_node("127.0.0.1", 5022, preset_node_list, bc2, chain_id=bc2.get_chain_identifier())

    # let server create two new blocks
    create_dummy_blocks(2, bc, priv_key)

    # let client create three new blocks
    create_dummy_blocks(3, bc2, priv_key)

    assert str(bc) != str(bc2)
    assert bc.get_length() < bc2.get_length()

    # add task
    node2.update_blockchains()

    # let it run for a moment
    env.start_reactor(5)

    # check if the update worked
    print("\n FIRST CHAIN:\n")
    print(str(bc))
    print("\n SECOND CHAIN:\n")
    print(str(bc2))

    assert str(bc) == str(bc2)


def test_mismatch_server_is_longer():
    # create test environment
    env = TestEnvironment()

    # setup first node
    bc, priv_key = Blockchain.Blockchain.init_blockchain()
    pub_key = bc.get_chain_pubkey()
    node1 = env.create_node("127.0.0.1", 5023, dict(), bc)
    preset_node_list = {node1.node_id: {"host": node1.interface, "port": node1.port, "timestamp": time.time()}}

    # add some blocks to the chain
    create_dummy_blocks(3, bc, priv_key)

    # make a copy of the chain
    bc2 = Blockchain.Blockchain()
    blocks = bc.get_blocks_starting_at_ID(0)
    for block in blocks:
        bc2.append_block(block)

    assert str(bc) == str(bc2)

    # create second node
    node2 = env.create_node("127.0.0.1", 5024, preset_node_list, bc2, chain_id=bc2.get_chain_identifier())

    # let server first create two new blocks
    create_dummy_blocks(2, bc, priv_key)

    # let client create one new block
    create_dummy_blocks(1, bc2, priv_key)

    assert str(bc) != str(bc2)
    assert bc.get_length() > bc2.get_length()

    # add task
    node2.update_blockchains()

    # let it run for a moment
    env.start_reactor(5)

    # check if the update worked
    print("\n FIRST CHAIN:\n")
    print(str(bc))
    print("\n SECOND CHAIN:\n")
    print(str(bc2))

    assert str(bc) == str(bc2)


def test_mismatch_server_is_longer_two_blocks():
    # create test environment
    env = TestEnvironment()

    # setup first node
    bc, priv_key = Blockchain.Blockchain.init_blockchain()
    pub_key = bc.get_chain_pubkey()
    node1 = env.create_node("127.0.0.1", 5025, dict(), bc)
    preset_node_list = {node1.node_id: {"host": node1.interface, "port": node1.port, "timestamp": time.time()}}

    # add some blocks to the chain
    create_dummy_blocks(3, bc, priv_key)

    # make a copy of the chain
    bc2 = Blockchain.Blockchain()
    blocks = bc.get_blocks_starting_at_ID(0)
    for block in blocks:
        bc2.append_block(block)

    assert str(bc) == str(bc2)

    # create second node
    node2 = env.create_node("127.0.0.1", 5026, preset_node_list, bc2, chain_id=bc2.get_chain_identifier())

    # let server first create three new blocks
    create_dummy_blocks(3, bc, priv_key)

    # let client create two new blocks
    create_dummy_blocks(2, bc2, priv_key)

    assert str(bc) != str(bc2)
    assert bc.get_length() > bc2.get_length()

    # add task
    node2.update_blockchains()

    # let it run for a moment
    env.start_reactor(5)

    # check if the update worked
    print("\n FIRST CHAIN:\n")
    print(str(bc))
    print("\n SECOND CHAIN:\n")
    print(str(bc2))

    assert str(bc) == str(bc2)