from basicblockchain import Block as bl
from basicblockchain import Transaction as t
from basicblockchain import Blockchain as bc
import time
from basicblockchain import VerificationTransaction as vt
import rsa

from basicblockchain.Sign import create_normal_signature, create_verify_signature

TEST_CHAIN_PATH = "test_chain.txt"
TEST_QUEUE_PATH = "test_queue.txt"


def test_chain():
    chain1, privkey = bc.Blockchain.init_blockchain(TEST_CHAIN_PATH, TEST_QUEUE_PATH) #grund chain verified
    chain1_pubkey = chain1.get_chain_pubkey()
    (pubkey1, privkey1) = rsa.newkeys(512)  # person1
    (pubkey2, privkey2) = rsa.newkeys(512)  # person2
    (pubkey3, privkey3) = rsa.newkeys(512)  # person3

    #Von person 1
    timestamp = time.time()
    v_sig1 = create_verify_signature(pubkey1, chain1_pubkey, timestamp, privkey)
    vt1 = vt.VerificationTransaction.create_verify_transaction(chain1, pubkey1, chain1_pubkey, timestamp, v_sig1)
    n_sig1 = create_normal_signature("dummyhash", pubkey1, timestamp, "Hello World 1", None, None, None, None, privkey1)
    nt1 = t.Transaction.create_transaction("dummyhash", pubkey1, timestamp, n_sig1, "Hello World 1")

    #Von Person 2
    timestamp2 = time.time()
    v_sig2 = create_verify_signature(pubkey2, chain1_pubkey, timestamp2, privkey)
    vt2 = vt.VerificationTransaction.create_verify_transaction(chain1, pubkey2, chain1_pubkey, timestamp2, v_sig2)
    n_sig2 = create_normal_signature("dummyhash", pubkey2, timestamp2, "Hello World 2", None, None, None, None, privkey2)
    nt2 = t.Transaction.create_transaction("dummyhash", pubkey2, timestamp2, n_sig2, "Hello World 2")

    #Von Person 3
    timestamp3 = time.time()
    v_sig3 = create_verify_signature(pubkey3, chain1_pubkey, timestamp3, privkey)
    vt3 = vt.VerificationTransaction.create_verify_transaction(chain1, pubkey3, chain1_pubkey, timestamp3, v_sig3)
    n_sig3 = create_normal_signature("dummyhash", pubkey3, timestamp3, "Hello World 3", None, None, None, None, privkey3)
    nt3 = t.Transaction.create_transaction("dummyhash", pubkey3, timestamp3, n_sig3, "Hello World 3")

    #Wieder von Person 1
    timestamp1 = time.time()
    #v_sig4 = create_verify_signature(pubkey1, chain1_pubkey, timestamp1, privkey)
    #vt4 = vt.Verification_Transaction.create_verify_transaction(chain1, pubkey1, chain1_pubkey, timestamp1, v_sig4)
    n_sig4 = create_normal_signature("dummyhash", pubkey1, timestamp1, "Hello World 4", None, None, None, None, privkey1)
    nt4 = t.Transaction.create_transaction("dummyhash", pubkey1, timestamp1, n_sig4, "Hello World 4")



    chain1.add_transactions_to_queue([vt1,vt2])
    bl.Block(chain1)

    chain1.add_transactions_to_queue([nt1,nt2])
    bl.Block(chain1)

    chain1.add_transactions_to_queue([])
    bl.Block(chain1)

    chain1.add_transactions_to_queue([vt2, nt3, nt2, nt1, vt1])
    #print(chain1.print_queue())
    #print(chain1)

    chain1.store_chain()
    chain1.store_queue()

    chain2 = bc.Blockchain.load_chain(TEST_CHAIN_PATH, TEST_QUEUE_PATH)

    assert str(chain1) == str(chain2)
    assert chain1.print_queue() == chain2.print_queue()
