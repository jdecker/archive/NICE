"""
Test p2p_models.py
:author: Jero
:last_modified: 05.06.2020
"""
import json

from p2p.p2p_models import NodeList, Message, MessageHeader, from_json
from p2p.p2p_server import generate_node_id


def test_p2p_models():
    # Instantiate a peers message (node list exchange)
    node_list = NodeList(generate_node_id())
    node_list.connection_successful(generate_node_id(), "localhost", 12345)  # add some exemplary connection
    msg = Message()
    msg.set_header(MessageHeader.PEERS_MSG)
    msg.set_body(node_list)

    # Simulate encoding, decoding (network transfer)
    # print(msg.to_json())
    json_msg = msg.to_json()
    json_msg = from_json(json_msg)
    # print(json_msg)

    # Check header
    assert json_msg["header"] is not MessageHeader.PEERS_MSG  # is comparison does not work, instead
    assert MessageHeader(json_msg["header"]) is MessageHeader.PEERS_MSG  # this should hold if everything worked
    assert json_msg["header"] == MessageHeader.PEERS_MSG  # this should work too

    # Check body
    assert json_msg["body"] == node_list.repr_json()
    print(type(json_msg["body"]))
    for peer in json.loads(json_msg["body"]):
        print(peer.split(":"))

    # Check node list merge
    node_list_2 = NodeList(generate_node_id())
    node_list_2.merge(node_list.node_list)
    assert node_list_2.node_list == node_list.node_list
    assert node_list.equals(node_list_2) and node_list_2.equals(node_list)
    print(node_list.to_string(), node_list_2.to_string())

    # Check node list merge after network transfer
    node_list_3 = NodeList(generate_node_id())
    node_list_3.merge(json.loads(json_msg["body"]))
    assert node_list_3.node_list == node_list.node_list
    assert node_list.equals(node_list_3) and node_list_3.equals(node_list)
    print(node_list_3.node_list)