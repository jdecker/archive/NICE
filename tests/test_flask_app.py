import logging

from flask import Flask
from flask_bootstrap import Bootstrap
from flask_uploads import configure_uploads

import block_logger
from webfrontend.frontend import frontend, nav, upload_set_audio, frontend_manager


def test_flask_app():
    # setup logger
    block_logger.setup_logger(console_level=logging.DEBUG)
    logger = logging.getLogger("block_logger")

    app = Flask(__name__)
    app.logger = logger

    # Initialize Bootstrap framework
    bootstrap = Bootstrap(app)
    # app.config["BOOTSTRAP_SERVE_LOCAL"] = True
    logger.debug("Initialized Bootstrap framework")

    # Register Blueprint for the frontend routes
    app.register_blueprint(frontend)
    logger.debug("Registered Blueprints")

    # Initialize Flask NavigationBar Extension
    nav.init_app(app)
    logger.debug("Initialized NavigationBar Extension")

    # Initialize CSRF
    # csrf = CsrfProtect(app)
    app.config["SECRET_KEY"] = "any secret string"
    logger.debug("Set secret key")

    # Flask-Uploads extension
    app.config['UPLOADS_DEFAULT_DEST'] = "webfrontend/static"
    app.config['MAX_CONTENT_LENGTH'] = 10 * 1024 * 1024     # limit uploads to 10 MB
    configure_uploads(app, upload_set_audio)
    logger.debug(f"Configured upload set {upload_set_audio.name}")

    # FrontendManager (with blockchain)
    frontend_manager.init_blockchain("test_chain.txt", "test_queue.txt")
    app.logger.debug("Blockchain started")

    app.logger.debug("Starting app...")
    # print(app.url_map)
    # print(app.extensions["bootstrap"])
    # print(app.blueprints)
    app.run()
