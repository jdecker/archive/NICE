"""
A minimal demo for testing the ipfs wrapper with a local ipfs daemon.

Simply run the file to test if the installation works.

:author: Jonathan Decker
:last_modified: 25.5.2020
"""

import ipfshttpclient

# /ip4/127.0.0.1/tcp/5001 is the address of the local API Server
with ipfshttpclient.connect() as client:
    res = client.add('ipfs_demo.py')
    # Should print {'Name': 'ipfs_demo.py', 'Hash': 'QmQvvAYcziqUN7G9j3ztFKohRKLzLXPtYGyfLk3B7jMyr7', 'Size': '252'}
    print(res)
    # Should print this files content
    print(client.cat(res['Hash']))
