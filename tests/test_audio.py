"""
Some simple tests with mp3 files and the eyeD3 library.
:author: Jero
:last_modified: 10.07.2020
"""
import logging

import eyed3
import mutagen
from eyed3.mp3 import Mp3AudioFile

logger = logging.getLogger("block_logger")


def print_audio_metadata(audiofile):
    """
    Print metadata of an audio file
    :param audiofile: Audio file
    :type audiofile: Mp3AudioFile
    :return: None
    :rtype: None
    """
    print("Artist:", audiofile.tag.artist)
    print("Album:", audiofile.tag.album)
    print("Album Artist:", audiofile.tag.album_artist)
    print("Title:", audiofile.tag.title)
    print("Track Number:", audiofile.tag.track_num)


def test_audio():
    audio = eyed3.load("../exported_keys/Musway_Studio_-_001-2_-_Hello_My_Friend.mp3")
    print_audio_metadata(audio)

    file_type = mutagen.File("../exported_keys/Musway_Studio_-_001-2_-_Hello_My_Friend.mp3")
    if not file_type:
        print("File type could not be determined!")

    print(file_type.info.pprint())
    print(file_type.info.length, type(file_type.info.length))
