"""
This module provides the frontend routes for the Flask app

:author: Jero
:last_modified: 07.07.2020
"""
import hashlib
import logging
import os
import time
from datetime import datetime
from urllib.parse import urlparse

import rsa
from flask import render_template, Blueprint, abort, flash
from flask_navbar import Nav
from flask_navbar.elements import *
from flask_uploads import UploadNotAllowed
from jinja2 import TemplateNotFound
from rsa import PublicKey, PrivateKey
from twisted.internet import defer, task, reactor
from twisted.internet.defer import CancelledError
from twisted.python.failure import Failure
from werkzeug.datastructures import FileStorage

from webfrontend.IPFS_Node import IPFSNode
from basicblockchain.Block import Block
from basicblockchain.Blockchain import Blockchain
from basicblockchain.Transaction import Transaction
from basicblockchain.VerificationTransaction import VerificationTransaction
from webfrontend.forms import MusicSearchForm, UploadSongForm, upload_set_audio, VerifyContributorForm
from webfrontend.audio_search import search_song, get_length_from_audio

from p2p.p2p_server import State

# Get Logger
logger = logging.getLogger("block_logger")

# Init Blueprint
frontend = Blueprint("frontend", __name__, template_folder="templates", static_folder="static", static_url_path="")

# Init Flask Nav
nav = Nav()


# Register a menu bar that will be displayed at the top of the page
@nav.navigation("top")
def top_nav():
    """
    Get the navigation bar for the top of the pages
    :return:
    """
    return Navbar(
        View("NICE", ".show", page="index"),
        NavUl(
            View("Project description", ".show", page="about"),
            View("Installation instructions", ".show", page="install"),
            View("Listen to a song", ".show", page="listen"),
            View("Upload your own song", ".show", page="upload"),
            View("Verify a contributor", ".show", page="verify"),
            View("Inspect blockchain", ".show", page="chain"),
            View("Pending transactions", ".show", page="transactions")
        )
    )


# Show a page, e.g. Main page (index), Installation instructions page
@frontend.route("/", defaults={'page': 'index'})
@frontend.route("/<page>")
def show(page):
    """
    Show a html page from folder "templates"
    :param page: Name of the page
    :return: Rendered template
    """
    try:
        return render_template(f"{page}.html")
    except TemplateNotFound:
        abort(404)


@frontend.route("/about")
def about():
    """
    Show the about page
    :return: Rendered template
    """
    blockchain = frontend_manager.blockchain
    p2p_node = frontend_manager.p2p_node
    p2p_host = p2p_node.server_host
    p2p_port = p2p_node.server_port
    return render_template("about.html", chain_id=blockchain.get_chain_identifier(),
                           host=p2p_host, port = p2p_port)


@frontend.route("/listen", methods=["GET", "POST"])
def listen():
    """
    Show the html page for searching for music
    :return: Rendered template
    """
    form = MusicSearchForm()
    if request.method == "POST":
        return search_results(form)
    else:
        return render_template("listen.html", form=form)


@frontend.route("/listen/results")
def search_results(form):
    """
    Show the search results for a music query
    :param form:
    :return: Rendered template
    """

    # search the transactions for the given parameter
    transaction_list = frontend_manager.blockchain.get_all_transactions()
    criteria = str(form.select.data)
    parameter = str(form.search.data)
    logger.debug(f"Searching for music with criteria={criteria}, parameter={parameter}")
    results = search_song(transaction_list, criteria, parameter)

    # Show results template
    if results:
        return render_template("results.html", form=form, results=results)
    else:
        flash("Your query did not return any results.")
        form = MusicSearchForm()
        return render_template("listen.html", form=form)


@frontend.route("/upload", methods=["GET", "POST"])
def upload():
    """
    Display form for song file submission
    :return:
    """
    form = UploadSongForm()
    if request.method == "POST":

        # Check if the form is valid
        if form.validate_on_submit():
            logger.debug("Flask upload_file() got a valid form")
            return frontend_manager.process_upload(form)
        else:
            logger.debug("Flask upload_file() got an invalid form!")
            flash(f'Cannot upload "{form.song.data.filename}": {", ".join(form.song.errors)}!', 'error')
            for error in form.errors:
                print(error)
        return render_template("upload.html", form=form)

    else:
        # show form to upload a song
        return render_template("upload.html", form=form)


@frontend.route("/upload/success")
def upload_file_success():
    """
    Display the upload success page.
    :return: Rendered upload success template
    """
    return show("upload_success")


@frontend.app_template_filter("to_url")
def url_song(hash):
    """
    Flask template filter that gets the IPFS url for the song
    with the given IPFS hash. This url can be used to play the
    song via the html page.
    Usage:
        <source src="{{ hash | to_url }}" type="[some_audio_type]"/>
    :param hash: IPFS hash
    :type hash: str
    :return: Url of the song
    :rtype: str
    """
    return urlparse(f"https://ipfs.io/ipfs/{hash}").geturl()


@frontend.route("/verify", methods=["GET", "POST"])
def verify():
    """
    Show the page where a contributor can be verified and process
    the VerifyContributorForm input.
    :return: Rendered verification template (Flask)
    """
    form = VerifyContributorForm()
    if request.method == "POST":
        return frontend_manager.verify_contributor(form)
    else:
        # Show page with form to verify a contributor
        return render_template("verify.html", form=form)


@frontend.route("/verify/success")
def verify_contributor_success():
    """
    Show the verification success page
    :return: Rendered verification success template
    """
    return show("verify_success")


@frontend.app_errorhandler(413)
def request_entity_too_large(error):
    """
    Display an error if the uploaded file is too large.
    :param error: Not used here
    :return: Error template for code 413
    """
    return 'File Too Large To Upload', 413


@frontend.route("/chain")
def inspect_chain():
    """
    Return the template to inspect the current blockchain.
    :return: Rendered template
    """
    blockchain = frontend_manager.blockchain.chain
    return render_template("chain.html", blockchain=blockchain)


@frontend.route("/transactions")
def pending_transactions():
    """
    Return the template to inspect the pending transactions.
    :return: Rendered template
    """
    transactions = frontend_manager.blockchain.q.queue  # This gives a copy of the current queue
    return render_template("transactions.html", transactions=transactions)


@frontend.app_template_filter("pretty_block")
def to_pretty_block(block):
    """
    Pretty print a block of the blockchain
    :param block: Block of the blockchain
    :type block: Block
    :return: Pretty printed block
    :rtype: str
    """
    return block.__str__()


@frontend.app_template_filter("pretty_tx")
def to_pretty_transaction(transaction):
    """
    Pretty print a transaction of a block from the blockchain
    :param transaction: Transaction of a block
    :type transaction: Transaction or VerificationTransaction
    :return: Pretty printed transaction
    :rtype: str
    """
    return transaction.__str__()


@frontend.app_template_filter("human_time")
def to_human_readable_time(timestamp):
    """
    Return a human readable string from the given timestamp.
    :param timestamp: Timestamp
    :type timestamp: int or float
    :return: Human readable time format as string
    :rtype: str
    """
    date = datetime.fromtimestamp(timestamp)
    return date.strftime('%Y-%m-%d %H:%M:%S')


class FrontendManager:
    """
    Class for keeping track of IPFS Node, Blockchain and p2p node,
    also serves as glue between frontend and other sections.
    """

    def __init__(self):
        """
        Constructor for "empty" frontend object, blockchain needs to be set
        via init_blockchain() method.
        """
        # IPFS Node (client)
        self.ipfs_node = IPFSNode()

        # Blockchain
        self.blockchain = None
        self.bc_private_key = None

        # P2P node
        self.p2p_node = None

        # Deferred (for block generation timer+cancellation)
        self.deferred = None
        self.event = None

    def init_blockchain(self, chain_path, queue_path, chain_id=None):
        """
        Initialize blockchain of the frontend.
        If a chain file path is provided, check first if the chain can be loaded from the file.
        If no chain id is provided, the chain is created from scratch.
        If a chain id is provided and the chain could not be loaded from the file, an empty chain is
        created which will be replaced by the real chain received from a peer.
        :param chain_path: Path to the blockchain file or None (if it should not be loaded/stored)
        :type chain_path: str or None
        :param queue_path: Path to the transaction queue file (relative to this file or absolute) or None
        :type queue_path: str or None
        :param chain_id: ID of the NICE blockchain or None if a new chain should be created.
        :type chain_id: str or None
        :return: None
        """
        # If chain file path is provided, try to load from it
        self.blockchain = None
        self.bc_private_key = None
        if chain_path:
            try:
                self.blockchain = Blockchain.load_chain(chain_path, queue_path)
                if self.blockchain.get_length() <= 0:
                    logger.debug(f"Loaded empty chain from file {chain_path}")
                    self.blockchain = None  # Empty chain loaded

            except FileNotFoundError as e:
                logger.debug(f"Could not load chain from file {chain_path}: {e}")
                self.blockchain = None

        # If chain was not loaded (or empty)
        if not self.blockchain:

            # If no chain id is provided, create a new chain from scratch
            if not chain_id:
                self.blockchain, self.bc_private_key = Blockchain.init_blockchain(chain_path, queue_path)  # new chain
                self.export_initial_keys()
            # Else create an empty chain, will be requested from other peers
            else:
                self.blockchain = Blockchain(chain_path, queue_path)

    def init_p2p_node(self, p2p_node):
        """
        Initialize the p2p node.
        :param p2p_node: P2P node object
        :type p2p_node: ServerResponseFactory
        :return: None
        """
        self.p2p_node = p2p_node

    def add_event(self, event):
        self.event = event

    def upload_song_to_ipfs(self, form):
        """
        Upload the song from the form to Ipfs
        :param form: Form for uploading a song
        :type form: UploadSongForm
        :return: Hash of the song in Ipfs or None
        :rtype: (str, float) or None
        """
        filename = form.song.data.filename

        # Save the file locally at the flask server (directory: "webfrontend/static/uploads/")
        try:
            upload_name = upload_set_audio.save(form.song.data)
        except UploadNotAllowed as e:
            logger.error(f"Could not upload file to Flask: {e}")
            flash("Could not upload your song!", "error")
            return None

        # Get the song file path
        song_path = upload_set_audio.path(upload_name)
        logger.debug(f"Saved song locally to {song_path}")

        # Upload the file to IPFS
        ipfs_hash = self.ipfs_node.add_file(song_path)["Hash"]
        self.ipfs_node.client.pin.add(ipfs_hash)  # permanently adds this file, this is for later
        logger.debug(f'Uploaded {filename} to IPFS, got the hash "{ipfs_hash}"')

        # Get the length of the song
        length = get_length_from_audio(song_path)

        # Delete the locally stored song
        # os.remove(song_path)
        os.unlink(song_path)

        return ipfs_hash, length

    def process_upload(self, form):
        """
        Get the file from the form, try to upload it and show success/failure page.
        The file will be uploaded to the IPFS network and a new transaction containing
        the song's metadata (artist, title, year, ...) will be issued.
        :param form: Upload song form after submission
        :type form: UploadSongForm
        :return: Rendered template
        """

        # Get the keys
        con_public_key_file = form.con_public_key.data
        con_private_key_file = form.con_private_key.data

        con_public_key = self.read_public_key(con_public_key_file)
        con_private_key = self.read_private_key(con_private_key_file)
        logger.debug(f"Read keys: {con_public_key}, {con_private_key}")

        # Upload the song to Ipfs
        uploaded = self.upload_song_to_ipfs(form)
        if not uploaded:
            flash("Song could not be uploaded to Ipfs!", category="error")
            return render_template("upload.html", form=form)
        ipfs_hash, length = uploaded

        # Get the metadata from the form (title, interpreter, etc.)
        title = form.song_title.data
        interpreter = form.song_interpreter.data
        writer = form.song_writer.data
        producer = form.song_producer.data

        # Issue a new transaction to the blockchain
        transaction = self.make_transaction(
            ipfs_hash, con_public_key, con_private_key, title, interpreter, writer, producer, length
        )
        logger.debug(f"Made transaction {transaction}")

        # Add transaction to blockchain
        if transaction:
            self.blockchain.add_transactions_to_queue([transaction])
            logger.debug("Added transaction to queue")
        else:
            flash("Could not upload", "error")
            return render_template("verify.html", form=form)

        # Check transaction queue and generate new block(s) if necessary
        # self.check_transaction_queue()
        self.trigger_transaction_check()

        return upload_file_success()

    def verify_contributor(self, form):
        """
        Try to verify another contributor.
        :param form: User input to the contributor verification
        :type form: VerifyContributorForm
        :return: Rendered verification (success) template (Flask)
        """

        # Check if the form is valid
        if form.validate_on_submit():
            logger.debug("Flask verify_contributor() got a valid form")

            # Get keys from the form
            ver_public_key_file = form.ver_public_key.data
            ver_private_key_file = form.ver_private_key.data
            con_public_key_file = form.con_public_key.data

            # Load keys from the files
            ver_public_key = self.read_public_key(ver_public_key_file)
            ver_private_key = self.read_private_key(ver_private_key_file)
            con_public_key = self.read_public_key(con_public_key_file)
            logger.debug(f"Read keys: {ver_public_key}, {ver_private_key}, {con_public_key}")

            # Abort if a key could not be loaded
            if any(key is None for key in [ver_public_key, ver_private_key, con_public_key]):
                return render_template("verify.html", form=form)

            # Abort if trying to verify myself
            if ver_public_key == con_public_key:
                flash("Cannot verify myself again", "error")
                return render_template("verify.html", form=form)

            # Make a new verifying transaction
            transaction = self.make_verification_transaction(ver_public_key, ver_private_key, con_public_key)
            logger.debug(f"Made new verification transaction {transaction}")

            # Add to queue if not None
            if transaction:
                self.blockchain.add_transactions_to_queue([transaction])
            else:
                flash("Could not verify", "error")
                return render_template("verify.html", form=form)

            # Check transaction queue and generate new block(s) if necessary
            # self.check_transaction_queue()
            self.trigger_transaction_check()

            return verify_contributor_success()
        else:
            logger.debug("Flask verify_contributor() got an invalid form!")
            if form.ver_public_key.errors:
                flash(f'Cannot verify: {", ".join(form.ver_public_key.errors)}!', 'error')
            if form.ver_private_key.errors:
                flash(f'Cannot verify: {", ".join(form.ver_private_key.errors)}!', 'error')
            if form.con_public_key.errors:
                flash(f'Cannot verify: {", ".join(form.con_public_key.errors)}!', 'error')
        return render_template("verify.html", form=form)

    def hash_transaction(self, ipfs_hash, con_public_key, timestamp, title, interpreter, writer, producer, length):
        """
        Compute the hash for a transaction
        :param ipfs_hash: Hash for song in IPFS
        :type ipfs_hash: str
        :param con_public_key: Public key of the contributor
        :type con_public_key: PublicKey
        :param timestamp: Timestamp from time.time()
        :type timestamp: float
        :param title: Title of the song
        :type title: str
        :param interpreter: Interpreter of the song
        :type interpreter: str
        :param writer: Writer of the song (optional)
        :type writer: str or None
        :param producer: Producer of the song (optional)
        :type producer: str or None
        :param length: Length of the song in seconds (optional)
        :type length: float or None
        :return: Hash of the transaction
        :rtype: str
        """
        string = str(ipfs_hash) + str(con_public_key) + str(timestamp) + str(title) + str(interpreter) \
                 + str(writer) + str(producer) + str(length)
        return hashlib.sha256(string.encode()).hexdigest()

    def make_transaction(self, ipfs_hash, con_public_key, con_private_key, title, interpreter, writer, producer,
                         length):
        """
        Compute the hash for a transaction
        :param ipfs_hash: Hash for song in IPFS
        :type ipfs_hash: str
        :param con_public_key: Public key of the contributor
        :type con_public_key: PublicKey
        :param con_private_key: Private key of the contributor
        :type con_private_key: PrivateKey
        :param title: Title of the song
        :type title: str
        :param interpreter: Interpreter of the song
        :type interpreter: str
        :param writer: Writer of the song (optional)
        :type writer: str or None
        :param producer: Producer of the song (optional)
        :type producer: str or None
        :param length: Length of the song in seconds (optional)
        :type length: float or None
        :return: Song transaction or None
        :rtype: Transaction or None
        """
        # Transaction hash
        timestamp = time.time()
        tx_hash = self.hash_transaction(ipfs_hash, con_public_key, timestamp,
                                        title, interpreter, writer, producer, length
                                        )
        logger.debug(f"Transaction hash {tx_hash}")

        # Obtain signature (signed hash)
        signature = rsa.sign(tx_hash.encode(), con_private_key, 'SHA-1')

        # Make transaction
        tx = Transaction.create_transaction(
            ipfs_hash, con_public_key, timestamp, signature, title, interpreter, writer, producer, length
        )
        return tx

    def hash_verification_transaction(self, con_public_key, ver_public_key, timestamp):
        """
        Compute the hash for the verification transaction.
        :param con_public_key: Contributor's public key
        :type con_public_key: PublicKey
        :param ver_public_key: Verifier's public key
        :type ver_public_key: PublicKey
        :param timestamp: Timestamp from time.time()
        :type timestamp: float
        :return: Hash of the verification transaction
        :rtype: str
        """
        hash_str = str(con_public_key) + str(ver_public_key) + str(timestamp)
        return hashlib.sha256((hash_str).encode()).hexdigest()

    def make_verification_transaction(self, ver_public_key, ver_private_key, con_public_key):
        """
        Make a new transaction for verifying another contributor
        :param ver_public_key: Verifier's public key
        :type ver_public_key: PublicKey
        :param ver_private_key: Verifier's private key
        :type ver_private_key: PrivateKey
        :param con_public_key: New contributor's public key
        :type con_public_key: PublicKey
        :return: Verification transaction to verify the new contributor
        :rtype: VerificationTransaction
        """
        # Hash of the transaction
        timestamp = time.time()
        hash = self.hash_verification_transaction(con_public_key, ver_public_key, timestamp)

        # Obtain signature (signed hash)
        signature = rsa.sign(hash.encode(), ver_private_key, 'SHA-1')

        # Verification Transaction
        transaction = VerificationTransaction.create_verify_transaction(
            self.blockchain, con_public_key, ver_public_key, timestamp, signature
        )
        return transaction

    def read_public_key(self, public_key_file):
        """
        Read public key from file
        :param public_key_file: File with public key
        :type public_key_file: FileStorage
        :return: The public key that was read or None if an
                    exception occured while loading
        :rtype: PublicKey or None
        """
        key_data = public_key_file.read()
        try:
            return rsa.PublicKey.load_pkcs1(key_data)
        except Exception as e:
            logger.warning(
                f"Exception occurred when trying to load public key from file {public_key_file.filename}: {e}")
            flash(f"Could not load public key from file {public_key_file.filename} (exception {e})")
        return None

    def read_private_key(self, private_key_file):
        """
        Read private key from file
        :param private_key_file: File with private key
        :type private_key_file: FileStorage
        :return: The private key that was read or None if an
                    exception occured while loading
        :rtype: PrivateKey or None
        """
        key_data = private_key_file.read()
        try:
            return rsa.PrivateKey.load_pkcs1(key_data)
        except Exception as e:
            logger.warning(
                f"Exception occurred when trying to load private key from file {private_key_file.filename}: {e}")
            flash(f"Could not load private key from file {private_key_file.filename} (exception {e})")
        return None

    def write_key(self, filename, key):
        """
        Write the given (public/private) key to a file (PEM format).
        :param filename: Name of the file (will be located in folder exported_keys)
        :type filename: str
        :param key: Public or private key
        :param key: PublicKey or PrivateKey
        :return:
        """
        export_folder = os.getcwd()
        export_folder = os.path.join(export_folder, "exported_keys")
        file_path = os.path.join(export_folder, filename)
        with open(file_path, "wb+") as f:
            f.write(key.save_pkcs1("PEM"))
        logger.debug(f"Wrote key {key} to file {file_path}")

    def export_initial_keys(self):
        """
        Export initially created public and private key in PEM format.
        Location will be "NICE\exported_keys\"
        :return: None
        :rtype: None
        """
        self.write_key("chain_private.pem", self.bc_private_key)

        bc_public_key = self.blockchain.get_chain_pubkey()
        self.write_key("chain_public.pem", bc_public_key)
        logger.debug("Exported initial keys")

    def check_transaction_queue(self):
        """
        Checks if there are transactions in the Queue and either directly generates new blocks or
        starts a timer to create blocks when the time is up.
        This makes sure that the time between new blocks is limited as long as new transactions come in.
        This function must be called from the reactor thread and not the flask thread!
        :return: True if a new block was generated and this function should be called again.
        :rtype: bool
        """
        delay = 30  # 30 seconds timer
        logger.debug("Started check for transaction.")
        # check size of transaction Queue, if its at least MAX TRANSACTIONS,
        # check if a timer is already running and if yes, stop it, further
        # make a new Block and call this again afterwards via generate new block
        if self.blockchain.get_queue_size() >= Block.MAX_TRANSACTIONS:
            if self.deferred:
                logger.debug("Timer canceled.")
                self.deferred.cancel()
                self.deferred = None
            logger.debug("Generating a new Block.")
            self.generate_new_block()
            return True

        # else if there is at least one Transaction in the queue, check if a timer is already running
        # if no timer is running, set a timer
        # after the timer is up, a new block should be created and this function called again
        # if the timer is already running, log it and pass
        elif self.blockchain.get_queue_size() > 0:
            if self.deferred:
                logger.debug("Timer is already running.")
            else:
                self.deferred = task.deferLater(reactor, delay, self.generate_new_block)
                self.deferred.addErrback(self.timer_errback)
                logger.debug(f"Timer started for {delay} seconds")
            return False

    def trigger_transaction_check(self):
        """
        Makes sure that the transaction check is performed in the reactor thread, such that the timer can be used.
        :return: No return
        """
        logger.debug("Setting event.")
        self.event.set()

    def generate_new_block(self):
        """
        Trigger the generation of a new block for the blockchain.
        Further informs other nodes of the newly generated block.
        Loops and does not trigger informing other nodes while blocks are still generated.
        :return: No return
        """
        # If this was triggered by a timer or a timer is running, clear it
        if self.deferred:
            self.deferred.cancel()
            self.deferred = None

        # Make new block with Block.MAX_TRANSACTIONS transactions
        new_block = Block(self.blockchain)
        logger.debug(f"Added new_block {new_block}, {self.blockchain.get_queue_size()} transactions remaining")
        if not self.check_transaction_queue():

            # Notify other nodes about new block
            logger.debug(f"Notifiying other nodes about updated chain")
            deferred = defer.Deferred()
            for node_id, peer_host, peer_port in self.p2p_node.node_list.iterate():
                if (peer_host, peer_port) == (self.p2p_node.server_host, self.p2p_node.server_port):
                    continue

                # Notify node
                logger.debug(f"Notifying {node_id} ({peer_host}:{peer_port})")
                d = self.p2p_node.connect_to_peer(peer_host, peer_port, State.CLIENT_UPDATE_CHAIN)
                deferred.chainDeferred(d)


    def timer_errback(self, failure):
        """
        Errorback of the timer (if it got cancelled)
        :param failure: Failure in deferred timer
        :type failure: Failure
        :return: None
        :rtype: None
        """
        failure.trap(CancelledError)
        logger.debug("Timer was cancelled")
        self.deferred = None


frontend_manager = FrontendManager()
