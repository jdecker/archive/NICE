"""
Flask-WTForms for searching the music meta data, e.g. title, year, artist, etc.,
or for uploading a song to the NICE system.
:author: Jero
:last_modified: 18.06.2020
"""
from flask_uploads import UploadSet, AUDIO
from flask_wtf import FlaskForm
from flask_wtf.file import FileRequired, FileAllowed
from wtforms import SelectField, StringField, FileField, SubmitField, validators
from wtforms.validators import InputRequired, Length, Optional

from basicblockchain.Transaction import Transaction


class MusicSearchForm(FlaskForm):
    """
    Music search form class (Flask-WTForms)
    """
    choices = [("title", "Title"),
               ("interpret", "Interpret"),
               ("producer", "Producer"),
               ("writer", "Writer")]
    select = SelectField(label="Search ...", choices=choices, validators=[validators.Optional()], default="title")
    search = StringField("")
    submit = SubmitField(label="Search")


# Song (audio) uploads
upload_set_audio = UploadSet("uploads", AUDIO)


class UploadSongForm(FlaskForm):
    """
    Upload song form class (Flask-WTForms).
    Consists of a FileField called "song" with which audio files can be uploaded
    and a SubmitField called "submit" to submit the upload.
    """
    song = FileField(label="Your song", validators=[
        FileRequired(),
        FileAllowed(upload_set_audio,
                    f"Unsupported extension! (Supported extensions are {upload_set_audio.extensions})")
    ])

    # Song meta data
    max_length = Transaction.MAX_LEN
    song_title = StringField(label="Title", validators=[
        InputRequired(message="Title required"), Length(max=max_length, message=f"Title max. length {max_length}")
    ])
    song_interpreter = StringField(label="Interpreter", validators=[
        Optional(), Length(max=max_length, message=f"Interpreter max. length {max_length}")
    ])
    song_writer = StringField(label="Writer", validators=[
        Optional(), Length(max=max_length, message=f"Interpreter max. length {max_length}")
    ])
    song_producer = StringField(label="Producer", validators=[
        Optional(), Length(max=max_length, message=f"Interpreter max. length {max_length}")
    ])

    # Contributor keys
    con_public_key = FileField(label="Your public key",
                               validators=[FileRequired()])

    con_private_key = FileField(label="Your private key",
                                validators=[FileRequired()])

    submit = SubmitField("Upload")


class VerifyContributorForm(FlaskForm):
    """
    Verify a contributor form class (Flask-WTForms)
    """
    ver_public_key = FileField(label="Your public key",
                               validators=[FileRequired()])

    ver_private_key = FileField(label="Your private key",
                                validators=[FileRequired()])

    con_public_key = FileField(label="New contributor's public key",
                               validators=[FileRequired()])

    submit = SubmitField("Verify")
