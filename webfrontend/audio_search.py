"""
Search for audio files on the blockchain.
Takes the list of all transactions and searches them for any match in the given criteria.

:author: Sören Nienaber
:last_modified: 05.07.2020
"""
import mutagen
import logging

logger = logging.getLogger("block_logger")


def song_to_dict(song):
    """
    Creates a dictionary from a given song transaction.
    :param song: A transaction for a song.
    :type song: Transaction
    :return: A dictionary with fields for title, interpreter, producer, writer and the ipfs hash
    :rtype: dict
    """
    return {"title": song.title, "artist": song.interpreter, "producer": song.producer, "writer": song.writer,
            "hash": song.ipfs_hash}


def search_by_title(title, song):
    """
    The following methods match the parameter against the criterion.
    :param title: the search parameter
    :type title: str
    :param song: song to compare
    :type: Transaction
    :returns: a dict with the metadata of a matching song
    :rtype: dict
    """
    if song.title is not None:
        if title.upper() in song.title.upper():
            return song_to_dict(song)


def search_by_artist(artist, song):
    """
    The following methods match the parameter against the criterion.
    :param artist: the search parameter
    :type artist: str
    :param song: song to compare
    :type: Transaction
    :returns: a dict with the metadata of a matching song
    :rtype: dict
    """
    if song.interpreter is not None:
        if artist.upper() in song.interpreter.upper():
            return song_to_dict(song)


def search_by_writer(writer, song):
    """
    The following methods match the parameter against the criterion.
    :param writer: the search parameter
    :type writer: str
    :param song: song to compare
    :type: Transaction
    :returns: a dict with the metadata of a matching song
    :rtype: dict
    """
    if song.writer is not None:
        if writer.upper() in song.writer.upper():
            return song_to_dict(song)


def search_by_producer(producer, song):
    """
    The following methods match the parameter against the criterion.
    :param producer: the search parameter
    :type producer: str
    :param song: song to compare
    :type: Transaction
    :returns: a dict with the metadata of a matching song
    :rtype: dict
    """
    if song.producer is not None:
        if producer.upper() in song.producer.upper():
            return song_to_dict(song)


def search_song(songs, criteria, parameter):
    """
    Main method to search for songs on the blockchain. Takes every list entry and searches for the parameter in the
    given criterion.
    Then returns a list of all matching entries in a list.
    :param songs: list of transactions that contain the metadata of all songs
    :type songs: List[Transaction]
    :param criteria: search criterion, either title, interpret, writer or producer
    :type criteria: str
    :param parameter: search parameter
    :type parameter: str
    :returns: list of all matching songs
    :rtype: List[dict] or None
    """

    search_dict = {"title": search_by_title,
                   "interpret": search_by_artist,
                   "writer": search_by_writer,
                   "producer": search_by_producer}

    if criteria not in search_dict.keys():
        logger.error(f"Invalid search criteria, must be title, interpret, writer or producer but is {criteria}")
        return None

    func = search_dict.get(criteria)

    ret_songs = [*map(lambda x: func(parameter, x), songs)]
    ret_songs = [i for i in ret_songs if i is not None]
    return ret_songs


def get_length_from_audio(audio_file):
    """
    Get the length of the audio file if possible.
    :param audio_file: Path to the audio file
    :type audio_file: str
    :return: Length of the song in seconds (incl. milliseconds) or None
    :rtype: float or None
    """
    file_type = mutagen.File(audio_file)
    if not file_type:
        logger.debug("Could not determine file type")
        return None

    if file_type.info:
        return file_type.info.length

    return None
