"""
This file contains a class that is able to connect to a running IPFS node via the python module ipfshttpclient.
The class features the functionalities to connect to a node, upload a file to a node and pin that file on this node,
add a file to the IPFS network and pin it to the connected note, download a file via the Hash and close the connection
to the node.
:author: Claudius Neumann, Sören Nienaber
:time edited: 9.07.2020
"""

import ipfshttpclient
import io
import logging

logger = logging.getLogger("block_logger")


class IPFSNode:
    """
    Class for handling connection to the IPFS network.
    """

    def __init__(self):
        self.client = None

    def connect_to_network(self, network_addr=None):
        """
        This method connect to the general network, the option exists to connect to a private or specific cluster such
        that the possibility exists that the user of the software does not need to have the ipfs daemon run on their
        local machine and therefore does not need to contribute to the network
        :param network_addr: The address of the ipfs daemon, defaults to localhost:5001
        :type network_addr: str or None
        """
        try:
            if network_addr is not None:
                self.client = ipfshttpclient.connect(network_addr, session=True)
            else:
                self.client = ipfshttpclient.connect(session=True)
        except Exception as e:
            logger.error("Failed to connect to IPFS network.")
            logger.debug(str(e))
            raise BaseException("Could not connect to IPFS Daemon: " + str(e))

    def upload_file(self, filename, ipfs_name, pin_path):
        """
        This is used for a contributor to provide a file to the ipfs network. It writes the file into the contributors
        local repository and at the same time also pins the file to their repository such that a hash can be returned.
        This hash should later be either be shared through a blockchain or through some common files lying on each and
        every contributors file system. The hash needs to be shared through the pubsubs.
        :param filename: The name of the file on the local directory
        :type filename: str
        :param ipfs_name: The path where the file should be written to on the ipfs system itself, as it is only the
            local name this should not be a problem
        :type ipfs_name: str
        :param pin_path: The path to which it should be pinned. This should be done such that it is controlled through
            some kind of system
        :type pin_path: str
        :return: The hash dictonary with name, hash and size of the pinned file (and thus uploaded file)
        :rtype:
        """
        try:
            self.client.files.write(ipfs_name, filename, create=True)
            hash = self.client.add(pin_path)
            return hash
        except Exception as e:
            logger.error(f"Failed to upload file {filename} to the ipfs network.")
            logger.debug(str(e))
            raise "Could not Upload file: " + str(e)

    def add_file(self, filename):
        """
        This method simply pins a file to a local ipfs node such that from there it can be further shared.
        The method is intended to be used by special contributors to the net which have the node itself running in the
        background most of the times and thus provide some resource to the global NICE network. The file needs to be
        downloaded beforehand.
        when the file is actually pinned and not just downloaded
        :param filename: the filename which to add to the ipfs pin. This means, the file needs to be downloaded
            beforehand
        :type filename: str
        :return: A dictionary containing name, hash and size of the uploaded file
        :rtype: dict
        """
        try:
            hash = self.client.add(filename)
            return hash
        except Exception as e:
            logger.error(f"Failed to upload file {filename} to the ipfs network.")
            logger.debug(str(e))
            raise "Could not add file to network: " + str(e)

    def download_file(self, hash, target_filename):
        """
        This downloads a file through a given hash from the ipfs. The hash should be retrieved when the transaction
        has been confirmed and should also be properly encrypted such that the file can not be shared easily thus:
        SUGGESTION: Use the current blockchain hash as a encryption pattern and a with the transaction shared public key
        for the encryption
        :param hash: The hash of the file on the ipfs
        :type hash: str
        :param target_filename: The filename the downloaded file should be saved as
        :type target_filename: str
        :return: No return but the file should be written to the target_filename
        """
        try:
            bytes = self.client.cat(hash)
            with io.open(target_filename, 'wb') as f:
                f.write(bytes)
        except Exception as e:
            logger.error(f"Failed to download file {hash}")
            logger.debug(str(e))
            raise "Could not download file: " + str(e)

    def get_directory_on_network(self, dir):
        """
        Get a directory from the network. This should be used to discover which files are available on a local node for
        the later desktop client
        :param dir: The directory to be shown, when none is given, returns the root directory of the local node
        :type dir: str
        :return: returns a array of dictionaries of the files in the directory dir. Each entry in the array holds name,
        hash, size
        :rtype: List[dict]
        """
        try:
            return self.client.files.ls(dir)['Entries']
        except Exception as e:
            logger.error(f"Failed to find directory {dir}")
            logger.debug(str(e))
            raise "Could not find directory on network" + str(e)

    def get_hash(self, filename):
        """
        Adds a file to the ipfs network and returns the hash.
        :param filename: The name of the file to add.
        :type filename: str
        :return: The hash for the file
        :rtype: dict
        """
        hash = self.client.add(filename, only_hash=True)
        return hash

    def close(self):
        """
        Close the currently open session with the ipfs system.
        """
        self.client.close()


if __name__ == '__main__':
    test_node = IPFSNode()
    test_node.connect_to_network()
    # hash = test_node.add_file("requirements.txt")
    # print(test_node.get_directory_on_network("/"))
    # hash_2 = test_node.get_hash('abc')
    # print(hash_2)
    # test_node.download_file(hash)
    test_node.close()
