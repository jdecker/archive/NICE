"""
This class is the actual blockchain object implementation. Each Blockchain has a chain element
which is a list of blocks and a queue for the transactions that are not yet on
the chain.
:author: Adrian-Luca Stehr and Mattes Voigts
:last_modified: 09.07.2020
"""
import os

from basicblockchain import Block as Bl
from basicblockchain import VerificationTransaction as VTrans
from basicblockchain import Transaction as Trans
import json
import queue
import threading
import logging

logger = logging.getLogger("block_logger")


class Blockchain:
    """
    This class is the actual blockchain object. Each Blockchain has a chain element
    which is a list of blocks and a queue for the transactions that are not yet on
    the chain.
    """

    def __init__(self, chain_path=None, queue_path=None):
        """
        Constructor to build an empty Blockchain object. Does not have a genesis block and empty Queue.
        :param chain_path: Path to the blockchain file (relative to this file or absolute).
                           If this is None, the chain will not be stored.
        :type chain_path: str or None
        :param queue_path: Path to the transaction queue file (relative to this file or absolute)
                           If this is None, the queue will not be stored.
        :type queue_path: str or None
        """
        self.chain = []  # List of Blocks is considered to be the blockchain
        self.q = queue.Queue()  # Queue for waiting transactions
        self.lock = threading.Lock()  # lock to avoid race condition in multi-threaded env

        self.chain_path = chain_path
        self.queue_path = queue_path

    @classmethod
    def init_blockchain(cls, chain_path=None, queue_path=None):
        """
        Used to initialise a blockchain. Returns 2 values, the blockchain object and the privatekey of the initial
        verification transaction on the genesis block. That key is needed to verify the first contributor.
        :param chain_path: Path to the blockchain file (relative to this file or absolute).
                           If this is None, the chain will not be stored.
        :type chain_path: str or None
        :param queue_path: Path to the transaction queue file (relative to this file or absolute)
                           If this is None, the queue will not be stored.
        :return: Blockchain and the generated private key
        :rtype: Blockchain, PrivateKey
        """
        # Create a chain
        newchain = Blockchain(chain_path, queue_path)

        # Create genesis block
        genesis_block, privkey = Bl.Block.genesis_block()
        newchain.append_block(genesis_block, acquire_lock=False)

        # Store the chain if path given
        if chain_path:
            newchain.store_chain()

        return newchain, privkey

    def __str__(self):
        """
        To string method of a blockchain for debugging. Calls toString of each block
        in the chain
        :return: Returns string representation of the blockchain
        :rtype: str
        """
        s = ""
        for block in self.chain:
            s = "\n" + s + str(block)
        return s

    def get_chain_pubkey(self):
        """
        Return the public key of the initial verification transaction of the genesis block
        :return: Public key of the initial verification transaction of the genesis block
        :rtype: PublicKey
        """
        return self.chain[0].transactions[0].con_pubkey

    def print_queue(self):
        """
        Used to visualise the current state of the queue
        :return: String presentation of the queue
        :rtype: str
        """
        count = 0
        s = ""
        placeholder = "-----------------------------------\n"
        s = s + placeholder
        l = list(self.q.queue)
        for e in l:
            s = s + str(count) + " number in queue" + "\n" + str(e) + placeholder
            count = count + 1
        return s

    def add_transactions_to_queue(self, transactions):
        """
        Add multiple transactions given as list to the blockchains queue
        :param transactions: List of transactions
        :type transactions: list
        :return: None
        """
        for e in transactions:
            if type(e) == VTrans.VerificationTransaction or type(e) == Trans.Transaction:
                self.q.put(e)

        if self.queue_path:
            self.store_queue()
        return

    def get_transaction_from_queue(self):
        """
        Get the next transaction from the queue. Also removes it from the queue
        :return: The next Transaction of the queue
        :rtype: Transaction
        """
        trans = self.q.get()

        if self.queue_path:
            self.store_queue()
        return trans

    def get_transactions_from_queue(self, count):
        """
        Get the next n transactiosn from queue. If you want to remove more than there is, the queue will be empty
        afterwards
        :param count: Amount of transactions to be removed
        :type count: int
        :return: List of transactions
        that have been removed
        :rtype: list
        """
        tlist = []
        if self.q.qsize() < count:
            count = self.q.qsize()  # Wenn mehr aus der q geholt wird als drin ist, einfach alles holen
        while count > 0:
            tlist.append(self.q.get())
            count = count - 1

        if self.queue_path:
            self.store_queue()
        return tlist

    def get_queue_size(self):
        """
        Returns the current size of the queue meaning how many Transactiosn are in there stored
        :return: Amount of transactions in the queue
        :rtype: int
        """
        return self.q.qsize()

    def append_block(self, block, call_by_load=False, acquire_lock=True):
        """
        Verifies the given block before appending. If it is valid, it will be added to the blockchain
        May append change the block due to invalid transactions
        e.g. Verification of person 1 and Transaction of person 1 in the same block -> Only the verification is allowed
        and the transaction needs to take place later. Then it creates a new block and adds it with only the transaction.
        :param acquire_lock: Internal flag, used to manage multi-threaded writes to the blockchain.
        :type acquire_lock: bool
        :param block: Block to add to the chain
        :type block: Block
        :param call_by_load Indicates whether the function is called by the load_chain. If True, we do not want to update
                the stored_blockchain.txt
        :type call_by_load: bool
        :return: Nothing
        """
        # acquires the lock from chain to avoid parallel writing as the p2p server
        # and flask server might write at the same time
        try:
            if acquire_lock:
                self.lock.acquire()

            if block.id == 0:
                self.chain.append(block)
                if not call_by_load:
                    if self.chain_path:
                        self.update_stored_chain([block])
                if acquire_lock:
                    self.lock.release()
                return True
            elif self.verify_block(block, self.chain[block.id - 1].id, self.chain[block.id - 1].hash):
                self.chain.append(block)
                if not call_by_load:
                    if self.chain_path:
                        self.update_stored_chain([block])
                if acquire_lock:
                    self.lock.release()
                return True
            else:
                if acquire_lock:
                    self.lock.release()
                return False
        finally:
            # in case an error occurs, makes sure the lock is released anyway
            if acquire_lock and self.lock.locked():
                self.lock.release()

    def blocks_to_json(self, start, stop):
        """
        Transforms multiple block objects from the blockchain to json format by id.
        :param start: ID of first object to be transformed
        :type start: int
        :param stop: ID of last object to be transformed
        :type stop: int
        :return: Returns nested dictionary of objects from start to stop as json string
        :rtype: str
        """
        json_list = []
        for i in range(start, stop + 1):
            json_list.append(self.chain[i].block_to_json())
        json_dict = {}
        i = 0
        for e in json_list:
            json_dict[i] = e
            i = i + 1
        json_string = json.dumps(json_dict)
        return json_string

    def get_length(self):
        """
        Getter method for chain's length
        :return: length of the chain
        :rtype: int
        """
        return len(self.chain)

    def get_chain_identifier(self):
        """
        Getter method for chain's id (Hash of block 0)
        :return: Hash of block 0
        :rtype: str
        """
        return self.chain[0].hash

    def get_hash_at_ID(self, id):
        """
        Get the hash of a specific object on the blockchain
        :param ID: ID of the object we want to hash
        :type ID: int
        :return: Hash of the object
        :rtype: str
        """
        return self.chain[id].hash

    def get_time_stamp_at_ID(self, id):
        """
        Getter method for the timestamp of a specific block on the blockchain
        :param id: ID of the block which we want to get the timestamp of
        :type id: int
        :return: Return the timestamp in unix time
        :rtype: float
        """
        return self.chain[id].timestamp

    def get_block_at_ID(self, id):
        """
        Getter method for a specific block of the chain
        :param id: ID of the block we want to get
        :type id: int
        :return: Block at id
        :rtype: Block
        """
        return self.chain[id]

    def get_blocks_starting_at_ID(self, id):
        """
        Returns a list of blocks starting at a specific id to the end of the chain
        :param id: Starting ID. That will be the first blocks ID.
        :type id: int
        :return: A list of Blocks from starting ID to end
        :rtype: list
        """
        block_list = []
        while id < len(self.chain):
            block_list.append(self.chain[id])
            id = id + 1
        return block_list

    def get_newest_block(self):
        """
        Get the newest (last) block of the blockchain
        :return: The last Block of the chain
        :rtype: Block
        """
        return self.chain[self.get_length() - 1]

    def verify_block(self, block, previd, prevhash):
        """
        Verify that a block is valid and therefore not altered in any way.
        Checks whether:
        ID is upcounting,
        rehash = stored hash,
        previous rehashed = stored previous hash
        All contributors of the block are ok.
        :param block: The block we want to verify
        :type block: Block
        :return: Returns True if valid, False otherwise
        :rtype: bool
        """
        if block.id == 0:  # Block 0 kann nur sich selbst prüfen
            if block.hash == self.chain[0].hash_block():
                return True
            else:
                return False

        prevhash_correct = block.prevhash == prevhash
        id_correct = block.id == previd + 1
        hash_correct = block.hash == block.hash_block()
        valid_contributors = self.check_contributors(block)
        if prevhash_correct and id_correct and hash_correct and valid_contributors:
            return True
        else:
            return False

    def check_contributors(self, block):
        """
        We check for each transaction, whether it is allowed on the blockchain. Called in verify. And verify is called
        by append_block.
        Therefore, we may call append_block on an altered block, in case that some transactions are not allowed on the chain
        e.g. Verification of person 1 and Transaction of person 1 in the same block -> Only the verification is allowed
        and the transaction needs to take place later. Then it creates a new block and adds it with only the transaction.
        :param block: Block we want to check the contributors on
        :type block: Block
        :return: True if valid, else otherwise
        :rtype: bool
        """
        transaction_allowed = []
        for new_transaction in block.transactions:
            if type(new_transaction) == Trans.Transaction:
                if self.valid_verifier(new_transaction.con_pubkey):
                    transaction_allowed.append(True)
                else:
                    transaction_allowed.append(False)
            else:
                transaction_allowed.append(True)

        all_transactions_ok = True
        back_to_queue = []
        i = 0
        for e in transaction_allowed:
            if not e:
                all_transactions_ok = False
        if not all_transactions_ok:
            for e in transaction_allowed:
                if e:
                    back_to_queue.append(block.transactions[i])
                i = i + 1

            alt_block = Bl.Block.alternative_block(self, back_to_queue, block.timestamp)
            self.append_block(alt_block, acquire_lock=False)
        return all_transactions_ok

    def find_verification(self, con_pubkey):  # Is contributor verified?
        """
        Checks previous verification transactions, whether contributor is allowed to contribute
        :param con_pubkey: Contributors public key
        :type con_pubkey: PublicKey
        :return: True if valid, else otherwise
        :rtype: bool
        """
        for block in self.chain:
            for transaction in block.transactions:
                if type(transaction) == VTrans.VerificationTransaction:
                    if transaction.con_pubkey == con_pubkey:
                        return True
        return False

    def verify_blocks(self, blocks):
        """
        Iteratively verify a list of blocks. Checks whether the given has up-counting IDs
        and if there is a break it cuts the list at that point. On the remaining list of
        up-couting IDs, we check each block with verify_block. We return the last Block ID
        that is verified
        :param blocks: List of Blocks to verify
        :type blocks: list
        :return: The ID of the last verified block of the list, -1 if there is no verified block
        :rtype: int
        """
        start = blocks[0].id
        end = blocks[len(blocks) - 1].id
        compare = list(range(start, end + 1))  # up-counting array to compare id's with
        ascending = []
        i = 0
        for e in blocks:  # Build a new list of the correct up-counting id's
            if e.id == compare[i]:
                ascending.append(e)
                i = i + 1
            else:
                break
        last_correct_id = -1
        count = 0

        for e in ascending:  # For the up-counting blocks, verify each
            if not type(e) == Bl.Block:
                return last_correct_id
            if count == 0:  # First block needs to be handled seperately
                if not e.id == 0:  # genesis block handled seperately
                    for trans in e.transactions:  # for all transactions of that block
                        if type(trans) == VTrans.VerificationTransaction:  # if it is a verification transaction
                            if not (self.valid_verifier(trans.verifier_pubkey)):  # person that verifies that contributor is not allowed to
                                return last_correct_id
                        elif type(trans) == Trans.Transaction:  # it is a normal transaction
                            if not trans.check_transaction():  # transaction is not allowed in that form
                                return last_correct_id
                        else:
                            return last_correct_id
                if self.verify_block(e, self.chain[e.id - 1].id, self.chain[e.id - 1].hash):  # If first Block is valid
                    last_correct_id = e.id  # Save this as last block that is ok
                    count = count + 1  # Count up, we are no longer first block
                else:  # If first un-valid Block found
                    return -1
            else:  # same as above for other than genesis block
                if not e.id == 0:
                    for trans in e.transactions:
                        if type(trans) == VTrans.VerificationTransaction:
                            if not (self.valid_verifier(trans.verifier_pubkey)):
                                return last_correct_id
                        elif type(trans) == Trans.Transaction:
                            if not trans.check_transaction():
                                return last_correct_id
                        else:
                            return last_correct_id
                if self.verify_block(e, ascending[count - 1].id, ascending[count - 1].hash):  # verify others
                    last_correct_id = e.id
                    count = count + 1
                else:
                    return last_correct_id
        return last_correct_id  # Default: return last correct block if no un-valid was found

    def verify_chain(self):
        """
        Calls verify_blocks on the full chain
        :return: The ID of the last verified block of the list, False if there is no verified block
        :rtype: int, bool
        """
        return self.verify_blocks(self.chain)

    def replace_blocks(self, block_list, kill_orphans=False):
        """
        Replaces blocks in the blockchain with blocks from a list of blocks.
        The blocks in the blockchain get replaced with blocks that have the same block id.
        If kill_orphans is False we add all transactions from the blocks we lose to our transaction queue if it is True
        we lose them.
        If the highest block id in the blockchain is higher then the highest block id in blocklist we drop the blocks
        that have a higher id
        If the highest block id in the blockchain is lower then the highest block id in blocklist we add the additional
        blocks the blockchain
        :param block_list: the list of blocks we want to add to the blockchain
        :param kill_orphans: True when we want to lose the transactions in the blocks that get replaced otherwise False
        :type block_list: list<block>
        :type kill_orphans: bool
        :return True if everything went right. False and a block id if there is a none verified block
        :rtype bool + int
        """
        # acquires the lock from chain to avoid parallel writing as the p2p server
        # and flask server might write at the same time
        try:
            self.lock.acquire()
            id = block_list[0].id
            counter = 0  # The block in blocklist we are currently dealing with
            verified = self.verify_blocks(block_list)
            if verified != block_list[len(block_list) - 1].id:
                self.lock.release()
                return verified
            if not kill_orphans:
                while id + len(block_list) < len(self.chain):
                    self.add_transactions_to_queue(self.chain[len(self.chain) - 1].transactions)
                    self.chain.pop()
                while id < len(self.chain):
                    self.add_transactions_to_queue(self.chain[id].transactions)
                    self.chain[id] = block_list[counter]
                    counter = counter + 1
                    id = id + 1
            else:
                while id + len(block_list) < len(self.chain):
                    self.chain.pop()
                while id < len(self.chain):
                    self.chain[id] = block_list[counter]
                    counter = counter + 1
                    id = id + 1

            if self.chain_path:
                self.store_chain()
            while counter < len(block_list):
                self.append_block(block_list[counter], acquire_lock=False)
                counter = counter + 1
            self.lock.release()
            return verified
        # in case an error occurs, makes sure the lock is released anyway
        finally:
            if self.lock.locked():
                self.lock.release()

    def valid_verifier(self, verifier_pubkey):
        """
        Check whether a verifier is allowed to verify others
        :param verifier_pubkey: Verifier's public key
        :type verifier_pubkey: PublicKey
        :return: True is valid, else otherwise
        :rtype: bool
        """
        for block in self.chain:
            for trans in block.transactions:
                if isinstance(trans, VTrans.VerificationTransaction):
                    if verifier_pubkey == trans.con_pubkey:
                        return True
        return False

    def get_all_transactions(self):
        """
        Return all normal transactions as list that are currently on the blockchain
        :return: list of all normal transactions
        :rtype: list
        """
        t_list = []
        for block in self.chain:
            for trans in block.transactions:
                if type(trans) == Trans.Transaction:
                    t_list.append(trans)
        return t_list

    def store_chain(self):
        """
        Store the blockchain in the file known as self.chain_path.
        :return: None
        """
        json_chain = self.blocks_to_json(0, len(self.chain) - 1)
        dict = json.loads(json_chain)
        with open(self.chain_path, 'w') as file:
            for e_id, e_info in dict.items():
                file.write(e_info + "\n")
        return

    def update_stored_chain(self, blocks):
        """
        Update the file known as self.chain_path that holds the current chain.
        :param blocks: List of blocks to write to the file
        :type blocks: [Block]
        :return: None
        """
        with open(self.chain_path, 'a') as file:
            for e in blocks:
                json_block = e.block_to_json()
                file.write(json_block + "\n")
        return

    def store_queue(self):
        """
        Store the transaction queue in the file known as self.queue_file.
        :return: None
        """
        queue_list = list(self.q.queue)
        json_queue_list = []
        for e in queue_list:
            json_queue_list.append(e.transaction_to_json())
        with open(self.queue_path, 'w') as file:
            for e in json_queue_list:
                file.write(e + "\n")
        return

    @classmethod
    def load_chain(cls, chain_path, queue_path):
        """
        Load the Blockchain from a given file path.
        Also loads the transactions from the queue file
        :param chain_path: Path to the blockchain file
        :type chain_path: str
        :param queue_path: Path to the transaction queue file (relative to this file or absolute)
        :type queue_path: str
        :return: Blockchain that was loaded from file
        :rtype: Blockchain
        """
        chain = Blockchain(chain_path, queue_path)
        blocks = []

        with open(chain_path, 'r') as file:
            for line in file:
                blocks.append(Bl.Block.block_from_json(line))
        for e in blocks:
            chain.append_block(e, call_by_load=True)

        # Also load queue if file given and exists
        if queue_path and os.path.isfile(queue_path):
            chain.load_queue()
        return chain

    def load_queue(self):
        """
        Load the queue from the queue file known as self.queue_path
        into the current chain object.
        :return: None
        """
        with open(self.queue_path, 'r') as file:
            trans = Bl.Block.load_transactions(file.readlines())

            self.add_transactions_to_queue(trans)
        return
