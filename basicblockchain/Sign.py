"""
Small helper file to create signature for transactions.
As of now only used in test files.

:author: Adrian-Luca Stehr
:last_modified: 9.07.2020
"""


import hashlib
import rsa


def create_normal_signature(ipfshash, con_pubkey, timestamp, title, interpreter, writer, producer, length, privat_key):
    """
    Creates an rsa signature that is valid for a transaction based on the same parameters as given here.
    :param ipfshash: 
    :param con_pubkey: 
    :param timestamp: 
    :param title: 
    :param interpreter: 
    :param writer: 
    :param producer: 
    :param length: 
    :param privat_key: 
    :return: The rsa signature
    :rtype: bytes
    """
    string = str(ipfshash) + str(con_pubkey) + str(timestamp) + str(title) + str(
        interpreter) + str(writer) + str(producer) + str(length)
    message = hashlib.sha256(string.encode()).hexdigest()
    return rsa.sign(message.encode(), privat_key, 'SHA-1')


def create_verify_signature(con_pubkey, verifier_pubkey, timestamp, privat_key):
    """
    Creates an rsa signature that is valid for a transaction based on the same parameters as given here.
    :param con_pubkey:
    :param verifier_pubkey:
    :param timestamp:
    :param privat_key:
    :return: The rsa signature
    :rtype: bytes
    """
    string = str(con_pubkey) + str(verifier_pubkey) + str(timestamp)
    message = hashlib.sha256(string.encode()).hexdigest()
    return rsa.sign(message.encode(), privat_key, 'SHA-1')
