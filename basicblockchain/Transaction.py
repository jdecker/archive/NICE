"""
Transaction class to create transactions. That transactions can then be added to
the blocks which can then be added to the blockchain.

:author: Mattes Voigts and Adrian-Luca Stehr
:last_modified: 19.06.2020
"""

import hashlib
import rsa
import json


class Transaction:
    """
    Transaction class to create transactions. That transactions can then be added to
    the blocks which can then be added to the blockchain.
    """

    MAX_LEN = 30

    def __init__(self, ipfs_hash, con_pubkey, timestamp, signature, title, interpreter, writer, producer, length):
        """
        Use create_transaction instead, Constructor for transactions
        :param ipfs_hash: Hash we get from IPFS after uploading a song
        :param title: Title of the song
        :param interpreter: Individual who performed the song
        :param writer: Individual who wrote the song's text
        :param producer: Individual who produced the song
        :param length: Length of the song in seconds
        """
        self.ipfs_hash = ipfs_hash
        self.con_pubkey = con_pubkey
        self.timestamp = timestamp
        self.signature = signature
        self.title = title
        self.interpreter = interpreter
        self.writer = writer
        self.producer = producer
        self.length = length  # in seconds
        self.hash = None

    def __str__(self):
        """
        To string method for Transactions for better debugging
        :return: Returns a string presentation of a Transaction
        :rtype: str
        """
        hstring = "Hash = " + str(self.ipfs_hash)
        cstring = "| \t Contributor = " + str(self.con_pubkey) + "\n"
        tstring = "| \t Title = " + str(self.title)
        istring = "| \t Interpreter = " + str(self.interpreter)
        wstring = "| \t Writer = " + str(self.writer)
        pstring = "| \t Producer = " + str(self.producer)
        lstring = "| \t Length = " + str(self.length)
        s = hstring + cstring + tstring + istring + wstring + pstring + lstring
        return "[" + s + "]\n"

    @classmethod
    def create_transaction(cls, ipfs_hash, con_pubkey, timestamp, signature, title, interpreter=None, writer=None,
                           producer=None, length=None):
        """
        Uses the check_transaction function to check if the transaction is valid.
        If the Transaction is valid we return the Transaction else we return None.
        :param ipfs_hash: the ipfshash of the uploaded music
        :param con_pubkey: the public key of the contributor
        :param timestamp: the time at which the Transaction was created in the frontend as returned by time.time()
        :param signature: the singed hash of the Transaction
        :param title: the title of the music
        :param interpreter: the interpreter of the music
        :param writer: the writer of the music
        :param producer: the producer of the music
        :param length: the length of the music
        :type ipfs_hash: str
        :type con_pubkey: PublicKey
        :type timestamp: float
        :type signature: bytes
        :type title: str
        :type interpreter: str
        :type writer: str
        :type producer: str
        :type length: str
        :return: returns none if something went wrong else the Transaction
        :rtype: Transaction or None
        """
        transaction = Transaction(ipfs_hash, con_pubkey, timestamp, signature, title, interpreter, writer, producer,
                                  length)
        if not transaction.check_transaction():
            return None
        return transaction

    def hash_transaction(self):
        """
        hashes the Transaction using sha256. The hashed string consists of contributor public key, timestamp, title,
        interpreter, writer, producer and length in this order.
        We use the sha256 hash function
        :return: Returns a hash in hexadecimal.
        :rtype: str
        """
        string = str(self.ipfs_hash) + str(self.con_pubkey) + str(self.timestamp) + str(self.title) + str(
            self.interpreter) + str(self.writer) + str(self.producer) + str(self.length)
        self.hash = hashlib.sha256(string.encode()).hexdigest()
        return self.hash

    def check_transaction(self):
        """
        First checks if all meta information for the music are less then MAX_LEN characters each.
        After that we check the signature with the public key of the contributor to ensure that the transaction was
        singed with the private key of the public key we got. If both checks pass we return True else we return False.
        :return: True if everything is ok else False
        :rtype: bool
        """
        if self.title is not None:
            if type(self.title) == str:
                if len(self.title) > self.MAX_LEN:
                    return False
            else:
                return False
        else:
            return False

        if self.interpreter is not None:
            if type(self.interpreter) == str:
                if len(self.interpreter) > self.MAX_LEN:
                    return False
            else:
                return False

        if self.producer is not None:
            if type(self.producer) == str:
                if len(self.producer) > self.MAX_LEN:
                    return False
            else:
                return False

        if self.length is not None:
            if not type(self.length) == int:
                if not type(self.length) == float:
                    return False
        if not rsa.verify(self.hash_transaction().encode(), self.signature, self.con_pubkey):
            return False
        return True

    def transaction_to_json(self):
        """
        Writes the contents of the transaction to a dictionary and dumps it to a json string.
        :return: json string representing the transaction.
        :rtype: str
        """
        save_key = self.con_pubkey.save_pkcs1().decode()
        decode_signature = self.signature.hex()
        newdict = {
            "type": "transaction",
            "ipfshash": self.ipfs_hash,
            "con_pubkey": save_key,
            "signature": decode_signature,
            "title": self.title,
            "interpreter": self.interpreter,
            "timestamp": self.timestamp,
            "writer": self.writer,
            "producer": self.producer,
            "length": self.length
        }
        return json.dumps(newdict)

    @classmethod
    def transaction_from_json(cls, loaded_dict):
        """
        Creates a new transaction object based on the given dictionary.
        :param loaded_dict: A dictionary produced by json load on a json representation of a transaction.
        :type loaded_dict: dict
        :return: The loaded transaction.
        :rtype: Transaction
        """
        key_encoded = loaded_dict["con_pubkey"].encode()
        key = rsa.PublicKey.load_pkcs1(key_encoded)
        signature = bytes.fromhex(loaded_dict["signature"])

        newobj = cls.__new__(cls)
        newobj.ipfs_hash = loaded_dict["ipfshash"]
        newobj.con_pubkey = key
        newobj.signature = signature
        newobj.title = loaded_dict["title"]
        newobj.interpreter = loaded_dict["interpreter"]
        newobj.writer = loaded_dict["writer"]
        newobj.timestamp = loaded_dict["timestamp"]
        newobj.producer = loaded_dict["producer"]
        newobj.length = loaded_dict["length"]
        newobj.hash = newobj.hash_transaction()
        return newobj
