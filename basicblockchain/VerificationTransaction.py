"""
Transaction objects that contain information about verified contributors.
:author: Mattes Voigts, Adrian-Luca Stehr
:last_modified: 9.07.2020
"""

import json
import hashlib
import rsa


class VerificationTransaction:
    """
    Transaction objects that contain information about verified contributors.
    """

    def __init__(self, con_pubkey, verifier_pubkey, timestamp, signed_hash):
        """
        Constructor, use create verifiy transaction instead
        :param con_pubkey: Contributor's public key
        :type con_pubkey: PublickKey
        :param verifier_pubkey: Verifier's public key
        :type verifier_pubkey: PublicKey
        :param timestamp: Timestamp when the transaction was created.
        :type timestamp: float
        :param signed_hash: Signature created with rsa sign
        :type signed_hash: bytes
        """

        self.con_pubkey = con_pubkey
        self.verifier_pubkey = verifier_pubkey
        self.timestamp = timestamp
        self.signed_hash = signed_hash
        self.hash = None

    def __str__(self):
        """
        To string method for Verification_Transactions for better debugging
        :return: Returns a string presentation of a Verification_Transaction
        :rtype: str
        """
        genesis_verification = "genesis_verification".encode().hex()
        if self.signed_hash.hex() == genesis_verification:
            return "\n" + self.con_pubkey.save_pkcs1().decode() + "\n" + self.verifier_pubkey.save_pkcs1().decode() + \
                   "\n" + "GENESIS VERIFICATION" + "\n"

        string = "\n" + self.con_pubkey.save_pkcs1().decode() + "\n" + self.verifier_pubkey.save_pkcs1().decode() + \
                 "\n" + self.signed_hash.hex() + "\n"
        return string

    def hash_transaction(self):
        """
        Hashes the Verification_Transaction using sha256. The public key of both the contributor and the verifier
        together with the timestamp are used to create the string that gets hashed.
        :return: Returns a hash in hexadecimal.
        :rtype: str
        """
        hash_str = str(self.con_pubkey) + str(self.verifier_pubkey) + str(self.timestamp)
        self.hash = hashlib.sha256(hash_str.encode()).hexdigest()
        return self.hash

    @classmethod
    def create_verify_transaction(cls, chain, con_pubkey, verifier_pubkey, timestamp, signed_hash):
        """
        Verifies that the incoming information is correct by hashing the Verification_Transaction and using the
        public key of the verifier together with the signed_hash to verify that the Verification_Transaction was
        signed by the private key of the verifier. After that we check if the verifier public key
        is a verified contributor of our chain by revisiting all Verification_Transactions on the chain.
        if both conditions are matched the new Verification_Transaction is returned else we return none.
        :param chain: the chain the Verification_Transaction should be added to
        :param con_pubkey: the public key of the new contributor
        :param verifier_pubkey: the public key of the verifier
        :param timestamp: the time at which the transaction was created in the frontend as returned by time.time()
        :param signed_hash: the hash of the Verification_Transaction signed with the private key of the verifier
        :type chain: Blockchain
        :type con_pubkey: PublicKey
        :type verifier_pubkey: PublicKey
        :type timestamp: float
        :type signed_hash: bytes
        :return: returns none if something went wrong else the Verification_Transaction
        :rtype: VerificationTransaction or None
        """
        transaction = VerificationTransaction(con_pubkey, verifier_pubkey, timestamp, signed_hash)
        if rsa.verify(transaction.hash_transaction().encode(), signed_hash, verifier_pubkey):
            if chain.valid_verifier(transaction.verifier_pubkey):
                return transaction
            else:
                return None
        else:
            return None

    def transaction_to_json(self):
        """
        Writes the contents of the transaction to a dictionary and dumps it to a json string.
        :return: json string representing the transaction.
        :rtype: str
        """
        con_key = self.con_pubkey.save_pkcs1().decode()
        ver_key = self.verifier_pubkey.save_pkcs1().decode()
        decode_signature = self.signed_hash.hex()
        newdict = {
            "type": "verification",
            "con_pubkey": con_key,
            "verifier_pubkey": ver_key,
            "timestamp": self.timestamp,
            "signed_hash": decode_signature,
        }
        return json.dumps(newdict)

    @classmethod
    def transaction_from_json(cls, loaded_dict):
        """
        Creates a new transaction object based on the given dictionary.
        :param loaded_dict: A dictionary produced by json load on a json representation of a transaction.
        :type loaded_dict: dict
        :return: The loaded transaction.
        :rtype: Transaction
        """
        con_key_encoded = loaded_dict["con_pubkey"].encode()
        con_key = rsa.PublicKey.load_pkcs1(con_key_encoded)

        ver_key_encoded = loaded_dict["verifier_pubkey"].encode()
        ver_key = rsa.PublicKey.load_pkcs1(ver_key_encoded)

        signature = bytes.fromhex(loaded_dict["signed_hash"])

        newobj = cls.__new__(cls)
        newobj.con_pubkey = con_key
        newobj.verifier_pubkey = ver_key
        newobj.timestamp = loaded_dict["timestamp"]
        newobj.signed_hash = signature
        newobj.hash = newobj.hash_transaction()
        return newobj
