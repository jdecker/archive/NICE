"""
Block class to create block objects. These blocks are filled with transactions
and are later added to the blockchain
:author: Adrian-Luca Stehr and Mattes Voigts
:last_modified: 9.07.2020
"""

import time
import json
from basicblockchain import Transaction as Trans
import hashlib
import rsa
from basicblockchain import VerificationTransaction as VTrans
import logging

logger = logging.getLogger("block_logger")


class Block:
    """
    Block class to create block objects. These blocks are filled with transactions
    and are later added to the blockchain.
    """

    MAX_TRANSACTIONS = 5

    def __init__(self, chain):
        """
        Constructor to build a Block object
        :param chain: The blockchain we want to put that Block on
        :type chain: Blockchain
        """
        # acquires the lock from chain to avoid parallel writing as the p2p server
        # and flask server might write at the same time
        try:
            chain.lock.acquire()
            self.transactions = []  # create of that block list of transactions
            for e in chain.get_transactions_from_queue(
                    self.MAX_TRANSACTIONS):  # Add each given transaction to the Block
                self.transactions.append(e)
            self.timestamp = time.time()  # current unix time
            self.id = chain.get_newest_block().id + 1  # ID = previous ID + 1
            self.prevhash = chain.get_newest_block().hash_block()  # hash of previous block
            self.magic_number = None
            self.hash = self.proof_of_work()
            chain.append_block(self, acquire_lock=False)
            chain.lock.release()
        # in case an error occurs, makes sure the lock is released anyway
        finally:
            if chain.lock.locked():
                chain.lock.release()

    @classmethod
    def genesis_block(cls):
        """
        Create the genesis block of a blockchain. Called in init_blockchain but not by the constructor.
        Further generates a new rsa key pair for the first validator of the chain.
        The public key is part of the first transaction and the private key is returned along the Block.
        :return: Returns genesis block with ID=0, empty transaction list, timestamp, prevhash=None and magic number = 42
                 and the private key of the first validators key pair.
        :rtype: (Block, Privatkey)
        """
        (pubkey, privkey) = rsa.newkeys(512)
        init_block = cls.__new__(cls)
        init_block.id = 0
        init_block.timestamp = time.time()
        init_block.transactions = [
            VTrans.VerificationTransaction(pubkey, pubkey, init_block.timestamp, "genesis_verification".encode())]
        init_block.prevhash = None
        init_block.magic_number = 42  # The first number is not important
        init_block.hash = init_block.hash_block()
        # The privat key is only returned but not saved on the chain
        return init_block, privkey

    def __str__(self):
        """
        To string method for better debugging
        :return: String presentation of the Block
        :rtype: str
        """
        tstring = ""
        for tx in self.transactions:
            if type(tx) == Trans.Transaction:
                tstring = tstring + "Transaction = \n"
                tstring = tstring + str(tx) + "\n"
            elif type(tx) == VTrans.VerificationTransaction:
                tstring = tstring + "Verification Transaction = \n"
                tstring = tstring + str(tx) + "\n"

        hstring = "\t Hash = " + str(self.hash)
        phstring = "\t Previous Hash = " + str(self.prevhash)
        idstring = " | \t Block ID = " + str(self.id)
        seperator = "----------------------------------------------------------------"

        s = tstring + hstring + phstring + idstring + "\n" + seperator + "\n"
        return s

    @classmethod
    def alternative_block(cls, chain, transactions, timestamp):
        """
        Creates a block with a subset of transactions of another block. Called in Blockchain.check_contributors if
        some transactions are invalid.
        :param chain: chain we want to put the block on
        :type chain: Blockchain
        :param transactions: transaction list for the block
        :type transactions: list
        :param timestamp: timestamp of the block
        :type timestamp: float
        :return: The new block
        :rtype: Block
        """
        alt_block = cls.__new__(cls)
        alt_block.transactions = transactions  # create of that block list of transactions
        alt_block.timestamp = timestamp  # current unix time
        alt_block.id = chain.get_newest_block().id + 1  # ID = previous ID + 1
        alt_block.prevhash = chain.get_newest_block().hash_block()  # hash of previous block
        alt_block.magic_number = None
        alt_block.hash = alt_block.proof_of_work()
        return alt_block

    def block_to_json(self):
        """
        Transform a block to a json / string
        :return: Json / String presentation of the Block
        :rtype: str
        """
        tl = []
        for tx in self.transactions:
            if type(tx) == Trans.Transaction:
                tl.append(tx.transaction_to_json())
            elif type(tx) == VTrans.VerificationTransaction:
                tl.append(tx.transaction_to_json())
        newdict = {
            "id": self.id,
            "timestamp": self.timestamp,
            "prevhash": self.prevhash,
            "magic_number": self.magic_number,
            "hash": self.hash,
            "transactions": tl
        }
        return json.dumps(newdict)

    @classmethod
    def blocks_from_json(cls, json_string):
        """
        Transform a set of jsons (given as type of str) back to objects
        :param json_string: A json string representation of multiple blocks as created by blocks to json
        :type json_string: str
        :return: A list of objects (Blocks)
        :rtype: list
        """

        dict = json.loads(json_string)
        objlist = []
        for e_id, e_info in dict.items():
            objlist.append(Block.block_from_json(e_info))
        return objlist

    @classmethod
    def block_from_json(cls, string):
        """
        Transform one json back to an object
        :param string: The json format of some Block object
        :type string: str
        :return: Returns the object as object
        :rtype: Block
        """
        newobj = cls.__new__(cls)
        loaded_dict = json.loads(string)
        tl = Block.load_transactions(loaded_dict["transactions"])

        newobj.transactions = tl
        newobj.id = loaded_dict["id"]
        newobj.timestamp = loaded_dict["timestamp"]
        newobj.magic_number = loaded_dict["magic_number"]
        newobj.hash = loaded_dict["hash"]
        newobj.prevhash = loaded_dict["prevhash"]
        return newobj

    @classmethod
    def load_transactions(cls, transaction_strings):
        """
        Given a list of transaction strings in json format, determines their type, loads them and returns a list of
        them.
        :param transaction_strings: List of json strings representing transacitons. :type transaction_strings:
        List[str] :return: List[Transaction]
        """
        trans = []
        for transaction_string in transaction_strings:
            loaded_transaction = json.loads(transaction_string)
            if loaded_transaction["type"] == "transaction":
                trans.append(Trans.Transaction.transaction_from_json(loaded_transaction))
            elif loaded_transaction["type"] == "verification":
                trans.append(VTrans.VerificationTransaction.transaction_from_json(loaded_transaction))
            else:
                logger.error(f"Invalid transaction type: {transaction_string}")
        return trans

    def proof_of_work(self):
        """
        Performs minimal Proof of Work algorithm:
        Change a magic number and rehash the block until it has a leading 0.
        :return: returns the hash
        :rtype: str
        """

        hash_str = str(self.prevhash) + str(self.timestamp) + str(id)
        for transaction in self.transactions:
            if transaction.hash is not None:
                hash_str = hash_str + transaction.hash
            else:
                hash_str = hash_str + transaction.hash_transaction()

        self.magic_number = 0
        hash_found = False
        hash_new = self.hash_block()
        while hash_found is False:
            if hash_new[:1] == "0":
                hash_found = True
            else:
                self.magic_number += 1
                hash_new = self.hash_block()
        return hash_new

    def hash_block(self):
        """
        Hashes a block to sha-256. Important for hashing are attributes:
        previous hash, timestamp, id, transactions, magic number
        :return: SHA-256 Hash of the block
        :rtype: str
        """
        hash_str = str(self.prevhash) + str(self.timestamp) + str(id)
        for transaction in self.transactions:
            if transaction.hash is not None:
                hash_str = hash_str + transaction.hash
            else:
                hash_str = hash_str + transaction.hash_transaction()
        return hashlib.sha256((hash_str + str(self.magic_number)).encode()).hexdigest()
