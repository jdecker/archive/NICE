"""
This is the config file for the NICE blockchain system.
It provides the configurations for
    - the flask web frontend
    - the ipfs daemon
    - the p2p node (port, interface, bootstrap list, node list file)
    - and the blockchain (id and file)

:author: Jero
:last_modified: 12.07.2020
"""

# Flask configuration
FLASK_ENABLED = True
FLASK_PORT = 5000       # default is 5000
FLASK_HOST = "127.0.0.1"

# Ipfs configuration
IPFS_ENABLED = True

# P2P configuration
P2P_PORT = 5005     # default is 5005
P2P_HOST = ""
P2P_BOOTSTRAP = [("141.5.107.8", 5005), ("141.5.107.75", 5005)]     # ignored if nodes_file is load
P2P_NODES_FILE = "p2p_nodes.json"      # file path relative to Project root or absolute

# Blockchain configuration
# Join a NICE network or create a new one?
BLOCKCHAIN_FILE = "stored_blockchain.json"    # file path relative to Project root or absolute
QUEUE_FILE = "stored_queue.json"     # file path relative to Project root or absolute
BLOCKCHAIN_ID = "9a16b433d0a4eda80691638c6d09ff3bece318917f64b6eae6b8433adec67e5b"    # If None, start new chain, else join existing chain; ignore this if chain is loaded from file
