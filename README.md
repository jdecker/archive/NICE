# What is NICE
NICE - New Intelligent Chain Economy is a blockchain based music sharing platform based on a minimal proof of work blockchain and IPFS. 
Everyone can connect to the webpage hosted on an instance of NICE and search for songs and stream them from the IPFS network but only verified contributors can upload new songs.
Each instance of NICE consists of a [Flask](https://github.com/pallets/flask) server, a peer to peer node based on [Twisted](https://github.com/twisted/twisted), an [IPFS](https://ipfs.io/) node and the NICE blockchain. 
The Flask server deploys a website which allows searching searching, listening and uploading music. The peer to peer node communicates with other NICE nodes and synchronizes the blockchain. Finally the IPFS node connects to the public IPFS network and is responsible for uploading and downloading music.

# How does it work
Whenever a song is uploaded to a NICE node, its metadata and the hash of the song is saved as a transaction. The song itself is uploaded to the public IPFS network, where it can be found again via the hash. Transactions are saved locally until they are added to a block. The block is attached to the blockchain and spread through the network via the peer to peer nodes.
Each transaction must be signed by a verified contributor using the private key of that contributor. New contributors can be verified by already verified contributors. The first verified contributor is the creator of the genesis block.

# Installation
1. Clone the repository to a folder.
2. Make sure Python 3.8+ is installed (older versions might work but aren't tested).
3. Install IPFS command line tool (instructions can be found [here](https://docs.ipfs.io/how-to/command-line-quick-start/))
4. Make sure ipfs is on your PATH and running
`ipfs daemon` starts the ipfs daemon.
6. Create a virtual env and activate it.
7. Install dependencies via 
`pip install -r requirements.txt`
8. Configure config.py (See Configuration)
9. Start the node by running 
`python main.py`

# Configuration
Configuration is handled via the config.py file. Here the project may be configured using python code syntax.
The default configuration should work out of the box but further adjustments can be made.

## Flask configuration
FLASK_ENABLED: Whether or not a flask server should be started. 
If set to false, no website will be available to add new blocks.

FLASK_PORT: The port on which the flask server should listen. When accessing the website via the browser,
the port needs to be appended to the address, for example "localhost:5000", to access the local node on port 5000.
The port has to be available and not preoccupied.

FLASK_HOST: The interface on which the flask server should listen. For use in LAN simply use "127.0.0.1" or "localhost".
Should also work with a public domain, but this is not tested.

## IPFS configuration
IPFS_ENABLED: Will attempt to start the IPFS daemon with the command "ipfs daemon" when set to true.
In case the ipfs daemon is already running, this can be set to false, but it can also be left on true,
the attempt to start the ipfs daemon again will fail, but the system will be able to use the running daemon anyway.

## P2P configuration
P2P_PORT: The port the p2p server should listen on. Over this port, the node will receive updates about the chain from other nodes.
The port has to be available and not preoccupied.

P2P_HOST: The interface on which the p2p server should run. Should be the same as FLASK_HOST.

P2P_BOOTSTRAP: List of interface:port pairs from other nodes. Here the interface and port of at least one other node should be entered,
to get started with the p2p network and learn about further nodes from it. If this is the only node or the first node in the network,
no change is needed.

P2P_NODES_FILE: The list of known nodes needs to be saved to a file, to persist beyond restarts of the node.
The path may either be relative to the project folder or absolute.

## Blockchain configuration
BLOCKCHAIN_FILE: The current state of the blockchain needs to be saved to a file, to persist beyond restarts of the node.
The path may either be relative to the project folder or absolute.

QUEUE_FILE: The current local queue of transactions needs to be saved to a file to persist beyond restarts of the node.
The path my either be relative to the project folder or absolute.

BLOCKCHAIN_ID: The id is the hash of the very first block of NICE blockchain.
To avoid merging two chains from different NICE blockchains on the same network, only nodes with the same blockchain id will exchanged messages.
Either set this to None, to create a new chain or to the blockchain id of an existing NICE blockchain.
This node will fetch the blockchain from another node.
In case the blockchain file already exists, the blockchain id from the loaded blockchain will be used instead and this option will be ignored.
So to force the creation of a new blockchain, the blockchain file must not exist.

# Usage

## Listening to music
Open up the webpage hosted on a NICE node and switch to search for music. Here one can search for songs by title, producer, writer or interpreter. Found music files can be diretcly streamed from the ipfs network.

## Uploading music
To upload music, one must be a verified contributor (see Verifying a contributor). On the webpage open upload your own music and fill out the form. The music file must be less than 20 MB in size and you must provide your public and private key. The private key is used to sign the transaction and the public is attached to the transaction so the signature can later be verified.
Transactions will be added to the local queue of this NICE node and can be see on the pending transaction tab.
After a short time or when 5 transactions have accumulated, a new block will be created and the music files become searchable.

## Verifying a contributor
When creating a new NICE blockchain, a new rsa pkcs#1 512 public private key pair is created and saved to a file. Any other public keys that are verified on the chain must also use rsa pkcs#1 with 512 bits. 
To generate such a pair Openssl and the python rsa library can be used. Simply install it via 
`pip install rsa`
Generate a new key pair and extract the public key from the private key with
`pyrsa-keygen --pubout public_key.pem -o private_key.pem 512` 
The pyrsa-priv2pub command should be available in an enviroment where the python rsa library is installed. Simply installing the rsa library on system level does not guarantee that the executables for the commands can easily be found.
To verifiy a contributor go to the verify a contributor tab on a NICE node and enter a verified public-private keypair and the public key of the new contributor. The new key must follow the above described standards.
The new public key will be valid to verifiy transaction after it has been processed into a new block as described in Uploading music.