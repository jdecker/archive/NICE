"""
Provides a p2p server that sends its node list to any incoming connection and expects one in response.
Must be run on its own.

:author: Jonathan Decker and Jero Schäfer
:last_modified: 11.6.2020
"""
import json
import logging
from enum import Enum
from uuid import uuid4, UUID

from twisted.internet import reactor, task, error as twisted_error, defer
from twisted.internet.endpoints import TCP4ClientEndpoint, connectProtocol
from twisted.internet.protocol import Factory, Protocol, connectionDone
from twisted.internet.defer import Deferred, DeferredList

from p2p.p2p_models import Message, MessageHeader, Ping, Pong, NodeList, NodeID, BlockPacket, IDAndHashPacket
from basicblockchain import Blockchain as Bc, Block as B

# setup logger
logger = logging.getLogger("block_logger")


class State(str, Enum):
    """
    Simple enumeration class for state machine logic
    """
    SERVER_WAIT = "SERVER_WAIT"
    CLIENT_INIT = "CLIENT_INIT"  # initial message exchange with server (node id and peers exchange)
    CLIENT_PING = "CLIENT_PING"  # start ping routine
    CLIENT_SENT_PING = "CLIENT_SENT_PING"
    CLIENT_EXCHANGE_PEERS = "CLIENT_EXCHANGE_PEERS"  # exchange peers message
    CLIENT_SENT_PEERS = "CLIENT_SENT_PEERS"

    # Consensus related states
    # client is a new node and needs the entire chain from the server
    CLIENT_NEW_NODE = "CLIENT_NEW_NODE"
    # client sent message to request chain
    CLIENT_REQUESTED_CHAIN = "CLIENT_REQUESTED_CHAIN"
    # client wants to update its chain, this is the first client state
    CLIENT_UPDATE_CHAIN = "CLIENT_UPDATE_CHAIN"
    # States to handle mismatch on same chain length
    # Server state, client and server have the same length but do not match, they need to find the older block
    SERVER_MISMATCH_SAME = "SERVER_MISMATCH_SAME"
    # Client state, client and server have the same length but do not match, they need to find the older block
    CLIENT_MISMATCH_SAME = "CLIENT_MISMATCH_SAME"
    # Server state, server and client need to find the root of the mismatch
    # and find who has the older first mismatching block
    SERVER_FIND_OLDER = "SERVER_FIND_OLDER"
    # Client state, server and client need to find the root of the mismatch
    # and find who has the older first mismatching block
    CLIENT_FIND_OLDER = "CLIENT_FIND_OLDER"
    # Server state, server has the older first mismatching block and will overwrite the client
    SERVER_IS_OLDER = "SERVER_IS_OLDER"
    # Client state, client has the older first mismatching block and will overwrite the server
    CLIENT_IS_OLDER = "CLIENT_IS_OLDER"
    # Server state, server has the younger first mismatching block and will be overwritten by the client
    SERVER_IS_YOUNGER = "SERVER_IS_YOUNGER"
    # Client state, server has the younger first mismatching block and will be overwritten by the client
    CLIENT_IS_YOUNGER = "CLIENT_IS_YOUNGER"
    # States to handle longer client and longer server
    # Server state, server has the longer chain and will add to the client after finding how much to send
    SERVER_IS_LONGER = "SERVER_IS_LONGER"
    # Client state, client has the longer chain and will add to the server after finding how much to send
    CLIENT_IS_LONGER = "CLIENT_IS_LONGER"
    # Server state, server has shorter chain and will receive from the client after finding how much to send
    SERVER_IS_SHORTER = "SERVER_IS_SHORTER"
    # Client state, client has shorter chain and will receive from the server after finding how much to send
    CLIENT_IS_SHORTER = "CLIENT_IS_SHORTER"
    # States to handle mismatch on different length server and client
    # Server state, server will overwrite client after finding the root of the mismatch
    SERVER_MISMATCH_IS_LONGER = "SERVER_MISMATCH_IS_LONGER"
    # Client state, client will overwrite server after finding the root of the mismatch
    CLIENT_MISMATCH_IS_LONGER = "CLIENT_MISMATCH_IS_LONGER"
    # Server state, server will be overwritten by client after finding the root of the mismatch
    SERVER_MISMATCH_IS_SHORTER = "SERVER_MISMATCH_IS_SHORTER"
    # Client state, client will be overwritten by sever after finding the root of the mismatch
    CLIENT_MISMATCH_IS_SHORTER = "CLIENT_MISMATCH_IS_SHORTER"
    # Server state, mismatch was found and the server is ready to be overwritten by the client
    SERVER_MISMATCH_FOUND_IS_SHORTER = "SERVER_MISMATCH_FOUND_IS_SHORTER"
    # Client state, mismatch was found and the client is ready to be overwritten by the server
    CLIENT_MISMATCH_FOUND_IS_SHORTER = "CLIENT_MISMATCH_FOUND_IS_SHORTER"
    # Server state, mismatch was found and the server is ready to overwrite the client
    SERVER_MISMATCH_FOUND_IS_LONGER = "SERVER_MISMATCH_FOUND_IS_LONGER"
    # Client state, mismatch was found and the client is ready to overwrite the sever
    CLIENT_MISMATCH_FOUND_IS_LONGER = "CLIENT_MISMATCH_FOUND_IS_LONGER"

    @classmethod
    def states(cls):
        """
        Set of all allowed states
        :return: State types
        :rtype: set
        """
        return set(item.value for item in cls)

    @staticmethod
    def is_valid(state):
        """
        Check whether the given state is valid.
        :param state: A state.
        :type state: State (str enum).
        :return: True if the state is valid, False otherwise.
        :rtype: bool
        """
        return state in State.states()


class P2PProtocol(Protocol):
    """
    A single instance of this protocol represents one connection between client and server.
    By using message headers and states, this protocol is used by both server and client.
    """

    def __init__(self, factory, start_as_server=True, state=None):
        self.factory = factory
        self.node_list = factory.node_list

        # Maps all handles to the respective message headers
        self.header_handler_dict = {MessageHeader.NODE_ID: self.handle_node_id_msg,
                                    MessageHeader.PEERS_MSG: self.handle_peers_msg,
                                    MessageHeader.PING: self.handle_ping,
                                    MessageHeader.PONG: self.handle_pong,
                                    MessageHeader.UPDATE_CHAIN: self.handle_update_chain,
                                    MessageHeader.SYNC: self.handle_sync,
                                    MessageHeader.MISMATCH_SAME: self.handle_mismatch_same,
                                    MessageHeader.SHORTER_SERVER: self.handle_shorter_server,
                                    MessageHeader.LONGER_SERVER: self.handle_longer_server,
                                    MessageHeader.MISMATCH_LONGER_SERVER: self.handle_mismatch_longer_server,
                                    MessageHeader.FIND_OLDER: self.handle_find_older,
                                    MessageHeader.SERVER_IS_OLDER: self.handle_server_is_older,
                                    MessageHeader.SERVER_IS_YOUNGER: self.handle_server_is_younger,
                                    MessageHeader.MISMATCH_SHORTER_SERVER: self.handle_mismatch_shorter_sever,
                                    MessageHeader.MISMATCH_FOUND_LONGER_SERVER: self.handle_mismatch_found_longer_server,
                                    MessageHeader.MISMATCH_FOUND_SHORTER_SERVER: self.handle_mismatch_found_shorter_server,
                                    MessageHeader.NEW_NODE: self.handle_new_node}

        # The peer of this TCP connection
        self.peer = None
        self.peer_id = None

        # My host and port for this TCP connection
        self.host = None
        self.port = None

        # My node_id
        self.id = self.factory.node_id

        self.chain_id = self.factory.chain_id

        # fields mostly used for consensus algorithm
        self.chain = self.factory.chain
        self.chain_length = self.chain.get_length()

        if start_as_server:
            self.state = State.SERVER_WAIT  # server responds upon connection establishment
        else:
            assert state is not None, "If not started as server, state must not be None."
            assert State.is_valid(state), f"Invalid state {state}, must be one of {State.states()}"

            self.state = state  # client will do something when connection is made

    def connectionMade(self):
        """
        On a successful connection, the server updates itself and its client in the node list and
        sends the node list to the client. The client does not send its node list
        as it is supposed to have no new nodes for the server by now.
        :return: None
        """
        # Remember thyself
        self.host = self.transport.getHost().host
        self.port = self.transport.getHost().port

        # Add new connection
        self.peer = self.transport.getPeer()
        logger.info(f"Connection from {self.peer} to {self.host}:{self.port}")

    def connectionLost(self, reason=connectionDone):
        """
        On disconnected log it.
        :param reason:
        :return: None
        """

        logger.info(f"Disconnected from {self.peer_id} [{self.peer}], reason={reason}.")

    def dataReceived(self, data):
        """
        On receiving data from the client, decode and merge it to node list, then disconnect.
        :param data: Bytes received from client.
        :type data: bytes
        :return: None
        """
        logger.debug(f"Received data: {data}")

        # Deserialize message
        msg = json.loads(data)

        # Ignore messages from unknown peers and incorrectly formatted messages
        # Check for valid header
        try:
            header = msg["header"]
            if not MessageHeader.is_valid(header):
                logger.warning(f'Invalid header {header}, ignoring incorrectly formatted message {msg}.')
                return
        except KeyError:
            logger.warning(f'Missing field "header", ignoring incorrectly formatted message {msg}.')
            return

        # Check for body
        try:
            body = msg["body"]
        except KeyError:
            logger.warning(f'Missing field "body", ignoring incorrectly formatted message {msg}.')
            return

        # Call handler based on Message header
        if header in self.header_handler_dict:
            handler = self.header_handler_dict.get(header)
            handler(body)
        else:
            logger.debug(f"Ignoring unknown message {msg}.")
        return

    def send_msg(self, msg):
        """
        Send a message.
        :param msg: Message to be sent.
        :type msg: Message
        :return: None
        :rtype: None
        """
        # data send must be bytes so encode is used
        data = msg.to_json().encode()
        logger.debug(f"Sending message {data}.")
        self.transport.write(data)

    def send_node_id(self):
        """
        Send a NodeID message that contains the own node id.
        :return: None
        :rtype: None
        """
        # Create message to be send
        msg = Message()
        msg.set_header(MessageHeader.NODE_ID)
        msg.set_body(NodeID(self.id, self.factory.server_host, self.factory.server_port, self.chain_id))

        # Send it
        self.send_msg(msg)

    def handle_node_id_msg(self, body):
        """
        Handle an incoming NodeID message (exchange of node id).
        :param body: Body of the NodeID message, should be a dict according to the NodeID class
        :type body: dict
        :return: None
        :rtype: None
        """
        # Ignore if not expecting a NodeID message
        if self.state not in [State.SERVER_WAIT, State.CLIENT_INIT, State.CLIENT_NEW_NODE]:
            logger.warning(f'Ignoring node id message in state {self.state}.')
            return

        # Ignore if not correctly formatted messages
        try:
            body = json.loads(body)
            peer_id = body["node_id"]
            server_host = body["server_host"]  # important for peers message exchange
            server_port = body["server_port"]  # important for peers message exchange
            chain_id = body["chain_id"]
        except KeyError:
            logger.warning(f'Missing field(s) "node_id", "server_host", "server_port" or "chain_id",'
                           f' ignoring incorrectly formatted node id message body {body}.')
            return

        # Check chain id if matches own chain id
        if self.chain_id != chain_id:
            logger.info(f"The peer {peer_id} has got a different chain ID {chain_id} than my chain ID {self.chain_id}")

            # If client, stop the connection
            if self.state is not State.SERVER_WAIT:
                self.transport.loseConnection()
                return
        else:
            # Add new peer
            self.peer_id = peer_id
            self.node_list.connection_successful(self.peer_id, server_host, server_port)

        # Respond with node id message if in SERVER_WAIT state
        if self.state is State.SERVER_WAIT:
            self.send_node_id()
        # Request chain if in CLIENT_NEW_NODE state
        elif self.state is State.CLIENT_NEW_NODE:
            self.request_chain()
            self.state = State.CLIENT_REQUESTED_CHAIN
        # Terminate connection
        else:
            self.transport.loseConnection()

    def request_chain(self):
        """
        Called by client when started in new node mode
        :return: None
        """
        msg = Message()
        msg.set_header(MessageHeader.NEW_NODE)

        body = IDAndHashPacket(ID=0, hash=0)
        msg.set_body(body)
        self.send_msg(msg)

    def handle_new_node(self, body):
        """
        Called upon receiving new node header, server should send entire blockchain,
        client should accept chain.
        :param body:
        :return:
        """
        if self.state == State.SERVER_WAIT:
            msg = Message()
            msg.set_header(MessageHeader.NEW_NODE)

            block_packet = BlockPacket(self.chain, 0, self.chain_length - 1)
            msg.set_body(block_packet)
            self.send_msg(msg)

        elif self.state == State.CLIENT_REQUESTED_CHAIN:
            block_list = B.Block.blocks_from_json(body)
            for block in block_list:
                self.chain.append_block(block)
            self.transport.loseConnection()

    def handle_sync(self, body):
        """
        In case they are already in sync, nothing to do.
        :param body:
        :return:
        """
        self.transport.loseConnection()

    def update_chain(self):
        """
        Client sends the update chain message to initialize the consensus algorithm
        :return: None
        """
        msg = Message()
        msg.set_header(MessageHeader.UPDATE_CHAIN)

        latest_block = self.chain.get_newest_block()
        latest_hash = latest_block.hash
        body = IDAndHashPacket(ID=self.chain_length - 1, hash=latest_hash)
        msg.set_body(body)

        self.send_msg(msg)

    def handle_update_chain(self, body):
        """
        Called by the server when receiving update chain header.
        Compare ID and hash from body with own ID and hash of newest block.
        :return: None
        """
        body = json.loads(body)
        remote_ID = body["ID"]
        remote_hash = body["hash"]
        if self.chain_length - 1 == remote_ID:
            # client and server are either in sync or have a same length mismatch
            if self.chain.chain[self.chain_length - 1].hash == remote_hash:
                # client and server are in sync, server needs to inform client, that no action is required
                msg = Message()
                msg.set_header(MessageHeader.SYNC)

                response_body = IDAndHashPacket(remote_ID, remote_hash)
                msg.set_body(response_body)
                self.send_msg(msg)

            else:
                # client and server have a mismatch on same length, they will now try to find their common chain
                self.state = State.SERVER_MISMATCH_SAME
                msg = Message()
                msg.set_header(MessageHeader.MISMATCH_SAME)
                previous_hash = self.chain.get_hash_at_ID(remote_ID - 1)

                response_body = IDAndHashPacket(remote_ID - 1, previous_hash)
                msg.set_body(response_body)
                self.send_msg(msg)

        elif self.chain_length - 1 < remote_ID:
            # server has shorter chain, client needs to check if there is a mismatch
            self.state = State.SERVER_IS_SHORTER
            msg = Message()
            msg.set_header(MessageHeader.SHORTER_SERVER)

            response_body = IDAndHashPacket(self.chain_length - 1, self.chain.get_hash_at_ID(self.chain_length - 1))
            msg.set_body(response_body)
            self.send_msg(msg)

        else:
            # server has longer chain
            if self.chain.chain[remote_ID].hash == remote_hash:
                # server is longer but no mismatch, client needs to be updated
                self.state = State.SERVER_IS_LONGER
                msg = Message()
                msg.set_header(MessageHeader.LONGER_SERVER)

                response_body = BlockPacket(self.chain, remote_ID, self.chain_length - 1)
                msg.set_body(response_body)
                self.send_msg(msg)

            else:
                # server and client have mismatch, server will persist
                self.state = State.SERVER_MISMATCH_IS_LONGER
                msg = Message()
                msg.set_header(MessageHeader.MISMATCH_LONGER_SERVER)
                previous_hash = self.chain.get_hash_at_ID(remote_ID - 1)

                response_body = IDAndHashPacket(remote_ID - 1, previous_hash)
                msg.set_body(response_body)
                self.send_msg(msg)

    def handle_mismatch_same(self, body):
        """
        Called whenever a message has the header MISMATCH_SAME
        :param body:
        :return:
        """
        # client needs to change state
        if self.state == State.CLIENT_UPDATE_CHAIN:
            self.state = State.CLIENT_MISMATCH_SAME

        body = json.loads(body)
        remote_ID = body["ID"]
        remote_hash = body["hash"]
        if remote_hash == self.chain.chain[remote_ID].hash:
            # the matching chain was found
            if self.state == State.CLIENT_MISMATCH_SAME:
                self.state = State.CLIENT_FIND_OLDER
                # client needs to send its oldest mismatching block
                msg = Message()
                msg.set_header(MessageHeader.FIND_OLDER)

                response_body = BlockPacket(self.chain, remote_ID + 1, remote_ID + 1)
                msg.set_body(response_body)
                self.send_msg(msg)

            elif self.state == State.SERVER_MISMATCH_SAME:
                self.state = State.SERVER_FIND_OLDER
                # server needs to request the oldest mismatching block from client
                msg = Message()
                msg.set_header(MessageHeader.FIND_OLDER)
                response_body = IDAndHashPacket(remote_ID + 1)
                msg.set_body(response_body)
                self.send_msg(msg)

            else:
                logger.error(
                    f"Illegal state. State should be {State.CLIENT_MISMATCH_SAME} or {State.SERVER_MISMATCH_SAME} but is {self.state}")
                self.transport.loseConnection()
        else:
            # the mismatch goes even deeper
            msg = Message()
            msg.set_header(MessageHeader.MISMATCH_SAME)
            previous_hash = self.chain.chain[remote_ID - 1].hash

            response_body = IDAndHashPacket(remote_ID - 1, previous_hash)
            msg.set_body(response_body)
            self.send_msg(msg)

    def handle_find_older(self, body):
        """
        Client needs to send its oldest mismatching Block given at the remote ID.
        Server needs to determine which block is older.
        :param body:
        :return:
        """
        if self.state == State.SERVER_MISMATCH_SAME:
            self.state = State.SERVER_FIND_OLDER

        if self.state == State.CLIENT_MISMATCH_SAME:
            self.state = State.CLIENT_FIND_OLDER

        if self.state == State.CLIENT_FIND_OLDER:
            # client should send its oldest mismatching Block
            body = json.loads(body)
            remote_ID = body["ID"]
            msg = Message()
            msg.set_header(MessageHeader.FIND_OLDER)

            response_body = BlockPacket(self.chain, remote_ID, remote_ID)
            msg.set_body(response_body)
            self.send_msg(msg)

        elif self.state == State.SERVER_FIND_OLDER:
            # message should contain the oldest mismatching Block
            packet = BlockPacket()
            packet.from_json(body)
            remote_block = packet.block_list[0]
            remote_time_stamp = remote_block.timestamp
            own_time_stamp = self.chain.chain[remote_block.id].timestamp

            if own_time_stamp <= remote_time_stamp:
                # Server is older
                self.state = State.SERVER_IS_OLDER
                msg = Message()
                msg.set_header(MessageHeader.SERVER_IS_OLDER)

                response_body = BlockPacket(self.chain, remote_block.id)
                msg.set_body(response_body)
                self.send_msg(msg)

            else:
                # Server is younger
                self.state = State.SERVER_IS_YOUNGER
                msg = Message()
                msg.set_header(MessageHeader.SERVER_IS_YOUNGER)

                response_body = IDAndHashPacket(remote_block.id)
                msg.set_body(response_body)
                self.send_msg(msg)

        else:
            logger.error(
                f"Illegal state. State should be {State.SERVER_FIND_OLDER} or {State.CLIENT_FIND_OLDER} but is {self.state}")
            self.transport.loseConnection()

    def handle_server_is_older(self, body):
        """
        Only send by server, holds the blocks the client should replace its chain with
        :param body:
        :return:
        """
        if self.state == State.CLIENT_FIND_OLDER:
            self.state = State.CLIENT_IS_YOUNGER

            packet = BlockPacket()
            packet.from_json(body)
            self.chain.replace_blocks(packet.block_list)

            if self.chain.get_queue_size() > 0:
                logger.debug("Setting event.")
                self.factory.event.set()

            self.transport.loseConnection()

    def handle_server_is_younger(self, body):
        """
        When called by client, it delivers the blocks the server should replace its chain with.
        When called by server, it asks the client to send blocks to replace its chain.
        :param body:
        :return:
        """
        if self.state == State.CLIENT_FIND_OLDER:
            # client is older and should send its blocks
            self.state = State.CLIENT_IS_YOUNGER
            body = json.loads(body)
            remote_ID = body["ID"]
            msg = Message()
            msg.set_header(MessageHeader.SERVER_IS_YOUNGER)

            response_body = BlockPacket(self.chain, remote_ID, self.chain_length - 1)
            msg.set_body(response_body)
            self.send_msg(msg)

        elif self.state == State.SERVER_IS_YOUNGER:
            package = BlockPacket()
            package.from_json(body)
            self.chain.replace_blocks(package.block_list)

            if self.chain.get_queue_size() > 0:
                logger.debug("Setting event.")
                self.factory.event.set()

            self.transport.loseConnection()
        else:
            logger.error(
                f"Illegal state. State should be {State.CLIENT_FIND_OLDER} or {State.SERVER_IS_YOUNGER} but is {self.state}")
            self.transport.loseConnection()

    def handle_shorter_server(self, body):
        """
        When called by server, the client has send its blocks to append to the servers chain.
        When called by the client, the server has send its newest ID and hash and the client needs to check for mismatches.
        In case of no mismatches, the client sends the missing blocks.
        :param body:
        :return:
        """
        if self.state == State.CLIENT_UPDATE_CHAIN:
            self.state = State.CLIENT_IS_LONGER

        if self.state == State.SERVER_IS_SHORTER:
            package = BlockPacket()
            package.from_json(body)
            # self.chain.replace_blocks(package.block_list)
            for block in package.block_list:
                self.chain.append_block(block)

            self.transport.loseConnection()
        elif self.state == State.CLIENT_IS_LONGER:
            # check for mismatch
            body = json.loads(body)
            remote_ID = body["ID"]
            remote_hash = body["hash"]
            if remote_hash == self.chain.chain[remote_ID].hash:
                # no mismatch, just update server
                msg = Message()
                msg.set_header(MessageHeader.SHORTER_SERVER)
                package = BlockPacket(self.chain, remote_ID + 1, self.chain_length - 1)

                msg.set_body(package)
                self.send_msg(msg)
            else:
                # mismatch occured
                self.state = State.CLIENT_MISMATCH_IS_LONGER
                msg = Message()
                msg.set_header(MessageHeader.MISMATCH_SHORTER_SERVER)
                previous_hash = self.chain.get_hash_at_ID(remote_ID - 1)

                response_body = IDAndHashPacket(remote_ID - 1, previous_hash)
                msg.set_body(response_body)
                self.send_msg(msg)

        else:
            logger.error(
                f"Illegal state. State should be {State.SERVER_IS_SHORTER} or {State.CLIENT_IS_LONGER} but is {self.state}")
            self.transport.loseConnection()

    def handle_longer_server(self, body):
        """
        Only used by client, when server is longer and no mismatch occurred, client is updated.
        :param body:
        :return:
        """
        if self.state == State.CLIENT_UPDATE_CHAIN:
            package = BlockPacket()
            package.from_json(body)
            self.chain.replace_blocks(package.block_list)

            if self.chain.get_queue_size() > 0:
                logger.debug("Setting event.")
                self.factory.event.set()

            self.transport.loseConnection()
        else:
            logger.error(
                f"Illegal state. State should be {State.CLIENT_UPDATE_CHAIN} but is {self.state}")
            self.transport.loseConnection()

    def handle_mismatch_shorter_sever(self, body):
        """
        Client and server keep checking if the mismatch ends and if it ends,
        the server says it found the mismatch and asks for blocks,
        the client says it found the mismatch and sends the blocks.
        :param body:
        :return:
        """
        if self.state == State.SERVER_IS_SHORTER:
            self.state = State.SERVER_MISMATCH_IS_SHORTER

        if self.state == State.SERVER_MISMATCH_IS_SHORTER:
            body = json.loads(body)
            remote_ID = body["ID"]
            remote_hash = body["hash"]
            # check if the mismatch continues
            if remote_hash == self.chain.chain[remote_ID].hash:
                # the mismatch has ended
                self.state = State.SERVER_MISMATCH_FOUND_IS_SHORTER
                msg = Message()
                msg.set_header(MessageHeader.MISMATCH_FOUND_SHORTER_SERVER)

                response_body = IDAndHashPacket(remote_ID + 1)
                msg.set_body(response_body)
                self.send_msg(msg)

            else:
                # the mismatch continues
                msg = Message()
                msg.set_header(MessageHeader.MISMATCH_SHORTER_SERVER)

                response_body = IDAndHashPacket(remote_ID - 1, self.chain.get_hash_at_ID(remote_ID - 1))
                msg.set_body(response_body)
                self.send_msg(msg)

        elif self.state == State.CLIENT_MISMATCH_IS_LONGER:
            body = json.loads(body)
            remote_ID = body["ID"]
            remote_hash = body["hash"]
            # check if the mismatch continues
            if remote_hash == self.chain.chain[remote_ID].hash:
                # the mismatch has ended, the client can send the blocks
                self.state = State.CLIENT_MISMATCH_FOUND_IS_LONGER
                msg = Message()
                msg.set_header(MessageHeader.MISMATCH_FOUND_SHORTER_SERVER)

                package = BlockPacket(self.chain, remote_ID + 1, self.chain_length - 1)
                msg.set_body(package)
                self.send_msg(msg)
            else:
                # the mismatch continues
                msg = Message()
                msg.set_header(MessageHeader.MISMATCH_SHORTER_SERVER)

                response_body = IDAndHashPacket(remote_ID - 1, self.chain.get_hash_at_ID(remote_ID - 1))
                msg.set_body(response_body)
                self.send_msg(msg)

        else:
            logger.error(
                f"Illegal state. State should be {State.SERVER_IS_SHORTER} or {State.SERVER_MISMATCH_IS_SHORTER} or"
                f" {State.CLIENT_MISMATCH_IS_LONGER}but is {self.state}")
            self.transport.loseConnection()

    def handle_mismatch_found_shorter_server(self, body):
        """
        When called by the server, it received blocks from client.
        When called by client, server requested blocks.
        :param body:
        :return:
        """
        if self.state == State.SERVER_MISMATCH_FOUND_IS_SHORTER:
            self.state = State.SERVER_MISMATCH_IS_SHORTER

        if self.state == State.SERVER_MISMATCH_IS_SHORTER:
            self.state = State.SERVER_MISMATCH_FOUND_IS_SHORTER

            package = BlockPacket()
            package.from_json(body)
            self.chain.replace_blocks(package.block_list)

            if self.chain.get_queue_size() > 0:
                logger.debug("Setting event.")
                self.factory.event.set()

            self.transport.loseConnection()

        elif self.state == State.CLIENT_MISMATCH_IS_LONGER:
            self.state = State.CLIENT_MISMATCH_FOUND_IS_LONGER
            body = json.loads(body)
            remote_ID = body["ID"]
            msg = Message()
            msg.set_header(MessageHeader.MISMATCH_FOUND_SHORTER_SERVER)

            package = BlockPacket(self.chain, remote_ID, self.chain_length - 1)
            msg.set_body(package)
            self.send_msg(msg)

        else:
            logger.error(
                f"Illegal state. State should be {State.SERVER_MISMATCH_IS_SHORTER} or {State.CLIENT_MISMATCH_IS_LONGER}"
                f" but is {self.state}")
            self.transport.loseConnection()

    def handle_mismatch_longer_server(self, body):
        """
        When called by client, client should check if mismatch continues.
        When called by server, server should check if mismatch continues.
        :param body:
        :return:
        """
        if self.state == State.CLIENT_UPDATE_CHAIN:
            self.state = State.CLIENT_MISMATCH_IS_SHORTER

        if self.state == State.CLIENT_MISMATCH_IS_SHORTER:
            body = json.loads(body)
            remote_ID = body["ID"]
            remote_hash = body["hash"]
            # check if the mismatch continues
            if remote_hash == self.chain.chain[remote_ID].hash:
                # the mismatch has ended
                self.state = State.CLIENT_MISMATCH_FOUND_IS_SHORTER
                msg = Message()
                msg.set_header(MessageHeader.MISMATCH_FOUND_LONGER_SERVER)

                response_body = IDAndHashPacket(remote_ID + 1)
                msg.set_body(response_body)
                self.send_msg(msg)

            else:
                # the mismatch continues
                msg = Message()
                msg.set_header(MessageHeader.MISMATCH_LONGER_SERVER)

                response_body = IDAndHashPacket(remote_ID - 1, self.chain.get_hash_at_ID(remote_ID - 1))
                msg.set_body(response_body)
                self.send_msg(msg)

        elif self.state == State.SERVER_MISMATCH_IS_LONGER:
            body = json.loads(body)
            remote_ID = body["ID"]
            remote_hash = body["hash"]
            # check if the mismatch continues
            if remote_hash == self.chain.chain[remote_ID].hash:
                # the mismatch has ended, the client can send the blocks
                self.state = State.SERVER_MISMATCH_FOUND_IS_LONGER
                msg = Message()
                msg.set_header(MessageHeader.MISMATCH_FOUND_LONGER_SERVER)

                package = BlockPacket(self.chain, remote_ID + 1, self.chain_length - 1)
                msg.set_body(package)
                self.send_msg(msg)
            else:
                # the mismatch continues
                msg = Message()
                msg.set_header(MessageHeader.MISMATCH_LONGER_SERVER)

                response_body = IDAndHashPacket(remote_ID - 1, self.chain.get_hash_at_ID(remote_ID - 1))
                msg.set_body(response_body)
                self.send_msg(msg)

        else:
            logger.error(
                f"Illegal state. State should be {State.CLIENT_UPDATE_CHAIN} or {State.CLIENT_MISMATCH_IS_SHORTER} or"
                f" {State.SERVER_MISMATCH_IS_LONGER}but is {self.state}")
            self.transport.loseConnection()

    def handle_mismatch_found_longer_server(self, body):
        """
        When received by the server, the server should send the blocks to update the client.
        When received by the client, the client has received the blocks to update.
        :param body:
        :return:
        """
        if self.state == State.CLIENT_MISMATCH_FOUND_IS_SHORTER:
            self.state = State.CLIENT_MISMATCH_IS_SHORTER

        if self.state == State.CLIENT_MISMATCH_IS_SHORTER:
            self.state = State.CLIENT_MISMATCH_FOUND_IS_SHORTER

            package = BlockPacket()
            package.from_json(body)
            self.chain.replace_blocks(package.block_list)

            if self.chain.get_queue_size() > 0:
                logger.debug("Setting event.")
                self.factory.event.set()

            self.transport.loseConnection()

        elif self.state == State.SERVER_MISMATCH_IS_LONGER:
            self.state = State.SERVER_MISMATCH_FOUND_IS_LONGER
            body = json.loads(body)
            remote_ID = body["ID"]
            msg = Message()
            msg.set_header(MessageHeader.MISMATCH_FOUND_LONGER_SERVER)

            package = BlockPacket(self.chain, remote_ID, self.chain_length - 1)
            msg.set_body(package)
            self.send_msg(msg)

        else:
            logger.error(
                f"Illegal state. State should be {State.SERVER_MISMATCH_IS_LONGER} or {State.CLIENT_MISMATCH_IS_SHORTER}"
                f" but is {self.state}")
            self.transport.loseConnection()

    def send_peers_msg(self):
        """
        Send a peers message that contains the nodes list.
        :return: None
        :rtype: None
        """
        # Create message to be send
        msg = Message()
        msg.set_header(MessageHeader.PEERS_MSG)
        msg.set_body(self.node_list)

        # Send it
        self.send_msg(msg)

    def handle_peers_msg(self, body):
        """
        Handle an incoming peers message (exchange of node list) by updating the
        own list of known peers.
        :param body: Body of the peers message, should be according to the NodeList class
        only a dictionary
        :type body: dict
        :return: None
        :rtype: None
        """
        # Merge the node lists and open connections to the new peers
        try:
            node_list = json.loads(body)
            new_peers = self.node_list.merge(node_list)
            if new_peers:
                for node_id, host, port in new_peers:
                    logger.debug(f'New peer? {node_id, host, port}')

                    # Skip myself & this peer as there is already a connection
                    if node_id in [self.id, self.peer_id]:
                        continue

                    # Establish a new connection
                    client = TCP4ClientEndpoint(reactor, host, port)  # , bindAddress=(self.host, self.port))
                    d = connectProtocol(client,
                                        P2PProtocol(self.factory, start_as_server=False,
                                                    state=State.CLIENT_INIT)
                                        )
                    logger.info(f"Node {self.id} established a new connection to new node at {host}:{port}")

        except ValueError as e:
            logger.warning(f'An exception occured when merging the peers message body, exception {e}, '
                           f'ignoring incorrectly formatted message body {body}.')
            return

        logger.debug("Finished merging.")

        if self.state == State.SERVER_WAIT:  # Respond as server
            self.send_peers_msg()
        else:
            self.transport.loseConnection()

    def send_ping(self):
        """
        Send a ping message to check whether the other node is still alive.
        :return: None
        :rtype: None
        """
        msg = Message()
        msg.set_header(MessageHeader.PING)
        msg.set_body(Ping())

        # Send it
        self.send_msg(msg)
        self.state = State.CLIENT_SENT_PING

    def handle_ping(self, ping):
        """
        Handle an incoming ping message by responding with pong and updating the
        corresponding peer's timestamp in the node list.
        :param ping: Ping message body
        :type ping: dict
        :return: None
        :rtype: None
        """
        # Ignore if not formatted correctly
        if ping != Ping().repr_json():
            logger.warning(f'Invalid ping body {ping}, expected {Ping().repr_json()}, '
                           f'ignoring incorrectly formatted ping message.')
            return

        # Update timestamp
        remote_host = self.peer.host
        remote_port = self.peer.port
        self.node_list.connection_successful(self.peer_id, remote_host, remote_port)

        # Respond with pong
        self.send_pong()

    def send_pong(self):
        """
        Send a ping message as response to the received ping message
        :return: None
        :rtype: None
        """
        # Create message to be sent
        msg = Message()
        msg.set_header(MessageHeader.PONG)
        msg.set_body(Pong())

        # Send it
        self.send_msg(msg)

    def handle_pong(self, pong):
        """
        Handle an incoming pong message by updating the corresponding
        peer's timestamp in the node list as it is still alive.
        :param pong: Pong message body
        :type pong: dict
        :return: None
        :rtype: None
        """

        # Ignore if not formatted correctly
        if pong != Pong().repr_json():
            logger.warning(f'Invalid pong body {pong}, expected {Pong().repr_json()}, '
                           f'ignoring incorrectly formatted ping message.')
            return

        # Ignore if not in CLIENT_SENT_PING state, i.e. did not send a ping to the server
        if self.state is not State.CLIENT_SENT_PING:
            logger.warning(f'Ignoring pong message in state {self.state}.')
            return

        # Update timestamp
        remote_host = self.peer.host
        remote_port = self.peer.port
        self.node_list.connection_successful(self.peer_id, remote_host, remote_port)

        # Close the connection
        logger.debug(f"Peer {self.peer_id} is alive.")
        self.transport.loseConnection()


class P2PProtocolFactory(Factory):
    """
    Factory for creating P2PProtocols.
    Is persistent across multiple connections.
    """

    def __init__(self, server_host, server_port, blockchain, start_as_server=True, chain_id=None,
                 node_list=None, node_list_path=None):
        """
        Initialize the factory.
        :param start_as_server: If set to False, this node starts as client
                                and tries to join an existing p2p network.
                                If set to True, this node starts as server
                                for a new p2p network.
        :type start_as_server: bool (Default=True)
        :param server_host: Address (host) of the node's TCP server, None defaults
                            to all interfaces
        :type server_host: str or None
        :param server_port: Port of the node's TCP server
        :type server_port: int or str
        :param blockchain: Provide a Blockchain object that should be handled by this factory. If none is provided,
                            a new blockchain is created.
        :type blockchain: Bc.Blockchain
        """
        self.start_as_server = start_as_server
        self.node_id = f"{server_host}:{server_port}"

        self.node_list = NodeList(self.node_id, node_list_path=node_list_path, preset_addresses=node_list)

        self.server_host = server_host
        self.server_port = int(server_port)

        self.chain = blockchain
        if self.chain.get_length() == 0:
            self.chain_id = chain_id
            if self.chain_id is None:
                logger.error(f"P2PProtocolFactory creation {server_host}:{server_port} failed."
                             f" Blockchain length can't be zero and chain id be None.")
        else:
            self.chain_id = self.chain.get_chain_identifier()

        self.event = None

    def buildProtocol(self, addr):
        """
        Build a P2PProtocol and returns it, used by endpoint.
        :param addr:
        :return: ServerResponseProtocol
        """
        return P2PProtocol(self, self.start_as_server)

    def check_new_peers(self):
        """
        Check for new peers by iterating over the list of known peers
        and exchange peers lists with these nodes.
        :return: None
        """

        logger.debug("Checking for new peers...")

        for node_id, peer_host, peer_port in self.node_list.iterate():
            if (peer_host, peer_port) == (self.server_host, self.server_port):
                continue

            logger.debug(f"Checking for new peers with node {node_id} at host {peer_host} and port {peer_port}")

            # Connect to peer to exchange node lists
            self.connect_to_peer(peer_host, peer_port, State.CLIENT_EXCHANGE_PEERS)

    def connect_to_peer(self, peer_host, peer_port, state):
        """
        Connect to the peer in the given client state and return a deferred
        :param peer_host: Peer host name (interface)
        :type peer_host: str
        :param peer_port: Peer port
        :type peer_port: int
        :param state: Client state
        :type state: State
        :return: Deferred
        :rtype: Deferred
        """
        client = TCP4ClientEndpoint(reactor, peer_host, peer_port)
        d = connectProtocol(client, P2PProtocol(self, start_as_server=False, state=state))
        d.addCallbacks(cb_client_action, p2p_log_errback)  # add callbacks to log information
        return d

    def discover_peers(self, bootstrap):
        """
        Discover other nodes and send a Node ID message to them.
        Important note: This adds the found nodes to the own node list.
        :param bootstrap: List of host and port tuples
        :type bootstrap: [(str, int)] or None
        :return: Deferred
        :rtype: Deferred
        """

        logger.debug("Running discover peers.")

        # Discover other nodes
        deferred = defer.Deferred()
        for peer_host, peer_port in bootstrap:
            # Skip myself
            if (peer_host, peer_port) == (self.server_host, self.server_port):
                continue

            # Connect to peer
            d = self.connect_to_peer(peer_host, peer_port, State.CLIENT_INIT)
            deferred.chainDeferred(d)

        # Add a looping call that frequently checks for new peers
        logger.debug("Starting loop discover peers every 60s.")
        loop = task.LoopingCall(self.check_new_peers)
        loop.start(60, now=True)  # run every 60 secs

        return deferred

    def update_chain(self, bootstrap):
        """
        Update the NICE blockchain with all other nodes
        :param bootstrap: List of bootstrap peers (will be added to this node's NodeList if available)
        :type bootstrap: List of (str,int)-tuples with host name and port numbers
        :return: Deferred list or Deferred
        :rtype: DeferredList or Deferred
        """
        # My interface (host) and port
        interface = self.server_host
        port = self.server_port

        # Update chain with other nodes
        defer_list = list()
        deferred = defer.Deferred()

        # If this chain is empty, request a chain from a peer, otherwise do a chain update
        if self.chain.get_length() <= 0:
            state = State.CLIENT_NEW_NODE
        else:
            state = State.CLIENT_UPDATE_CHAIN

        # Connect to all nodes, but only exchange with first responding node
        for node_id, peer_host, peer_port in self.node_list.iterate():
            # Skip myself
            if (interface, port) == (peer_host, peer_port):
                # logger.debug("P2P Bootstrapping, skipping myself")
                continue

            # Connect to peer to request chain
            client = TCP4ClientEndpoint(reactor, peer_host, peer_port)
            d = connectProtocol(client, P2PProtocol(self, start_as_server=False, state=state))

            if state == State.CLIENT_NEW_NODE:
                defer_list.append(d)    # add to defer list
            else:
                d.addCallbacks(cb_client_action, p2p_log_errback)
                deferred.chainDeferred(d)

        if state == State.CLIENT_NEW_NODE:
            # Add callbacks that fire on first response
            dl = defer.DeferredList(defer_list, fireOnOneCallback=True)
            dl.addCallbacks(cb_client_list, p2p_log_errback)
            return dl
        else:
            return deferred

    def add_event(self, event):
        """
        Add event to p2p node
        :param event: Threading event
        :return: threading.Event
        """
        self.event = event

def p2p_log_errback(failure):
    """
    Error callback that traps a refused connection
    :param failure: Failure returned by the deferred
    :type failure: Failure
    :return: None
    """
    failure.trap(twisted_error.ConnectionRefusedError)  # handle ConnectionRefusedError by skipping that peer
    logger.debug(f"P2P: Ignoring unavailable peer ")


def test_ping(p):
    """
    Callback to test the ping message transfer.
    :param p:
    :return:
    """
    task.deferLater(reactor, 5, p.send_ping)
    # print(p, p.factory.to_string())


def cb_client_action(protocol):
    """
    Client action callback that is called when the client
    established a connection to the server and performs the
    client action based on the previously set state.
    :param protocol: Protocol that was instantiated
    :type protocol: P2PProtocol
    :return:
    """
    logger.debug(f"Connected successfully to peer {protocol.peer_id} at {protocol.peer} in state {protocol.state}")
    if protocol.state in [State.CLIENT_NEW_NODE, State.CLIENT_INIT]:
        # init connection, send own id to join the network, optionally request chain (NEW_NODE)
        protocol.send_node_id()
    elif protocol.state is State.CLIENT_EXCHANGE_PEERS:
        protocol.send_peers_msg()
    elif protocol.state is State.CLIENT_PING:  # ping the other one if it is still alive
        protocol.send_ping()
    elif protocol.state is State.CLIENT_UPDATE_CHAIN:  # start consensus algorithm
        protocol.update_chain()
    else:
        logger.warning(f"State {protocol.state} is not allowed!")
        protocol.transport.loseConnection()


def cb_client_list(deferred_list_result):
    """
    This function is used as callback for a DeferredList,
    when only the first response should be used for further
    processing.
    :param deferred_list_result: Tuple fired by DeferredList when fireOnOneCallback is True
                                 with the first element being the result of the Deferred
                                 (should be a Protocol) which fired and the second element
                                 being the index in DeferredList of that Deferred
                                 (see DeferredList for detailed info)
    :type deferred_list_result: (Any, int)
    :return: The index of the deferred
    """
    protocol, index = deferred_list_result
    cb_client_action(protocol)
    return index


def generate_node_id():
    """
    Generate a new node id.
    :return: Node id
    :rtype: str
    """
    return str(uuid4())


def is_valid_uuid4(uuid):
    """
    Check if uuid is a valid UUID4.
    :param uuid: UUID to test
    :type uuid: str
    :return: True if uuid is a valid UUID, otherwise False.
    :rtype: bool
    """
    try:
        uuid_obj = UUID(uuid, version=4)
    except ValueError:
        return False

    return str(uuid_obj) == uuid
