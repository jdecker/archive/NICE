"""
Provides NodeList class which is essentially a dictionary with additional functions.
The dictionary in NodeList holds the address and port as the key in the form "address:port"
and the time of the latest successful connection as the unix timestamp value.

:author: Jonathan Decker and Jero Schäfer
:last_modified: 11.6.2020
"""

import datetime
import json
import logging
import os
import time
from enum import Enum
from basicblockchain import Block as B

logger = logging.getLogger("block_logger")


def from_json(json_node_list):
    """
    Loads a json file as a python dictionary.
    :param json_node_list: A valid json file.
    :type json_node_list: json str
    :return: The python dictionary loaded from the given json.
    :rtype: dict
    """
    return json.loads(json_node_list)


class MessageHeader(str, Enum):
    """
    Simple enumeration class for message types
    """
    PEERS_MSG = "PEERS"  # Exchange node list
    PING = "PING"  # Simple ping command
    PONG = "PONG"  # Answer to ping
    NODE_ID = "NODE_ID"  # Own Node id exchange (plus chain id)

    # Consensus related headers
    # Triggers the update chain protocol
    UPDATE_CHAIN = "UPDATE_CHAIN"
    # Informs the client that the server is already in sync
    SYNC = "SYNC"
    # When client and server have the same chain length but mismatch
    MISMATCH_SAME = "MISMATCH_SAME"
    # When Client and server have the same chain length and found their common chain,
    # they need to find out which chain to keep
    FIND_OLDER = "FIND_OLDER"
    # When Client and server have the same chain length and the server persists
    SERVER_IS_OLDER = "SERVER_IS_OLDER"
    # When Client and server have the same chain length and the client persists
    SERVER_IS_YOUNGER = "SERVER_IS_YOUNGER"
    # When the server has a longer chain than the client
    LONGER_SERVER = "LONGER_SERVER"
    # When client and server mismatch and the server is longer so the server persists
    MISMATCH_LONGER_SERVER = "MISMATCH_LONGER_SERVER"
    # When client and server mismatch and the position of the mismatch was found
    MISMATCH_FOUND_LONGER_SERVER = "MISMATCH_FOUND_LONGER_SERVER"
    # When the server has a shorter chain than the client
    SHORTER_SERVER = "SHORTER_SERVER"
    # When the server has a shorter chain than the client and mismatches with the client
    MISMATCH_SHORTER_SERVER = "MISMATCH_SHORTER_SERVER"
    # When the position of the mismatch was found and the client persists
    MISMATCH_FOUND_SHORTER_SERVER = "MISMATCH_FOUND_SHORTER_SERVER"
    # When a node joins for the first time and needs to download the entire chain
    NEW_NODE = "NEW_NODE"

    @classmethod
    def set(cls):
        """
        Set of all allowed message types
        :return: Message types
        :rtype: set
        """
        return set(item.value for item in cls)

    @staticmethod
    def is_valid(header):
        """
        Check whether the given header is valid.
        :param header: Message header.
        :type header: MessageHeader (str).
        :return: True if the header is valid, False otherwise.
        :rtype: bool
        """
        return header in MessageHeader.set()


class Message:
    """
    Basic message class, has a header and a body.
    """

    header = None
    body = None

    def set_header(self, header):
        # validate header
        assert header in MessageHeader.set(), f"Invalid header {header}, cannot create message"
        self.header = header

    def set_body(self, body):
        # assert that body is a subclass of MessageBody
        if not isinstance(body, MessageBody):
            logger.error(f"Invalid body object {body}, must be an instance of class MessageBody or any subclass of it")
            return

        self.body = body

    def to_json(self):
        """
        Dumps the message to a json string that can be sent via the network.
        :return: Json string dump of the message.
        :rtype: json str
        """
        msg_header = self.header
        msg_body = self.body.repr_json()
        return json.dumps({"header": msg_header, "body": msg_body})


class MessageBody:
    """
    Basic message body class, should be subclassed for any type of message (e.g. NodeList).
    A body must provide a repr_json() method that returns a JSON serializable representation
    of this body and a from_json() method that takes a JSON
    """

    def repr_json(self):
        pass

    def from_json(self, json_str):
        pass


class BlockPacket(MessageBody):
    """
    Wraps the blockchain into a a MessageBody such that it can be exchanged easily
    via the network.
    """

    def __init__(self, block_chain=None, start=None, end=None):
        self.chain = block_chain
        self.start = start
        self.end = end
        self.block_list = None

    def repr_json(self):
        if not self.end:
            self.end = self.chain.get_length() - 1
        if not self.start:
            self.start = 0
        return self.chain.blocks_to_json(self.start, self.end)

    def from_json(self, json_str):
        self.block_list = B.Block.blocks_from_json(json_str)


class IDAndHashPacket(MessageBody):
    """
    Message body only containing the ID and hash of a block. Used during consensus algorithm.
    """

    def repr_json(self):
        return json.dumps({"ID": self.ID, "hash": self.hash})

    def from_json(self, json_str):
        id_hash_dict = json.loads(json_str)
        self.ID = id_hash_dict["ID"]
        self.hash = id_hash_dict["hash"]

    def __init__(self, ID=None, hash=None):
        self.ID = ID
        self.hash = hash


class Ping(MessageBody):
    """
    Simple ping message body
    """

    def repr_json(self):
        return "ping"


class Pong(MessageBody):
    """
    Simple pong message body (response to ping)
    """

    def repr_json(self):
        return "pong"


class NodeID(MessageBody):
    """
    Message for exchanging the node id's at the beginning of the connection.
    Also contains the node's server host:port pair to be added to the peers
    list (not to be confused with the specific TCP connection host:port pair)
    and the id of the blockchain (in case of multiple NICE chains on the same network).
    """

    def __init__(self, node_id, server_host, server_port, chain_id):
        self.node_id = node_id
        self.server_host = server_host
        self.server_port = server_port
        self.chain_id = chain_id

    def repr_json(self):
        return json.dumps(dict(node_id=self.node_id, chain_id=self.chain_id,
                               server_host=self.server_host, server_port=self.server_port))


class NodeList(MessageBody):
    """
    Wraps the node_list dictionary with additional functionality into a MessageBody
    such that it can be exchanged easily via the network.

    The internal dict consists of node_ids as keys and a sub dictionary with host, port and timestamp as keys.
    """

    def __init__(self, my_id, node_list_path=None, preset_addresses=None):
        """
        Constructor for a NodeList
        :param my_id: ID of the NICE blockchain
        :type my_id: str
        :param node_list_path: Node list file path or None
        :type node_list_path: str or None
        :param preset_addresses: list of host-port tuples
        :type preset_addresses: List[(str,str)]
        """
        self.my_id = my_id
        self.node_list = dict()
        self.path = node_list_path  # where to save the node list
        if self.path:
            self.load()
        if type(preset_addresses) == dict:
            self.merge(preset_addresses)  # merge the preset addresses (bootstrap)

        if self.path:
            self.store()

    def iterate(self):
        """
        Returns a generator to iterate over all nodes containing
        addresses (hosts) and ports in the node list. Can be iterated as:

        for node in node_list.iterate()

        where node is a tuple containing (node_id, host, port)
            - node_id which is the id of the node as str
            - host which is the host address as str
            - port which is the host's port number as int
        :return: generator returning node tuples
        :rtype: (str, str, int)
        """
        for node_id in self.node_list:
            node = self.node_list[node_id]
            host = node["host"]
            port = node["port"]
            yield node_id, host, port

    @staticmethod
    def new_peer(host, port):
        """
        Create a new peer dictionary
        :param host: The address (host) of the new peer.
        :type host: str
        :param port: The port of the new peer.
        :type port: int or str
        :return: Dictionary for the new peer containing "host", "port" and "timestamp"
        :rtype: dict
        """
        return dict(host=host, port=int(port), timestamp=int(time.time()))

    def connection_successful(self, node_id, host, port):
        """
        Updates the timestamp for a given node in the node list or adds it
        if does not exist yet.
        :param node_id: The id of the peer.
        :type node_id: str
        :param host: The address (host) of a peer that just got a successful connection.
        :type host: str
        :param port: The port of a peer that just got a successful connection.
        :type port: int or str
        :return: True if the field was successfully updated, false if it wasn't.
        :rtype: bool
        """
        if node_id in self.node_list:
            node = self.node_list[node_id]
            node["timestamp"] = int(time.time())
        else:
            self.node_list[node_id] = self.new_peer(host, port)
        self.store()

    def last_successful_connection_unix(self, node_id):
        """
        Returns the unix timestamp of the last successful connection
        of the given peer, or None if the peer doesn't exist.
        :param node_id: The id of the peer.
        :type node_id: str
        :return: The unix timestamp for the given address port pair or None.
        :rtype: float or None
        """
        if node_id in self.node_list:
            node = self.node_list[node_id]
            return node["timestamp"]
        logger.error("The node list does not hold the peer and couldn't return the unix timestamp")
        logger.debug(f"The following key (node_id) was not found in the node list: {node_id}")
        return None

    def last_successful_connection_human(self, node_id):
        """
        Returns a human readable timestamp of the last successful connection
        on the given peer, or None if the peer doesn't exist.
        :param node_id: The id of the peer.
        :type node_id: str
        :return: A human readable timestamp for the given peer or None.
        :rtype: str or None
        """
        if node_id in self.node_list:
            time_stamp = self.last_successful_connection_unix(node_id)
            date = datetime.datetime.fromtimestamp(time_stamp)
            return date.strftime('%Y-%m-%d %H:%M:%S')
        logger.error("The node list does not hold the given peer and couldn't return the human readable timestamp")
        logger.debug(f"The following key (node_id) was not found in the node list: {node_id}")
        return None

    def repr_json(self):
        """
        Get a JSON serializable representation of this object, basically
        only return the node list as this is everything.

        This method will be called from the wrapping Message object.
        :return: Node list
        :rtype: str
        """
        return json.dumps(self.node_list)

    def merge(self, remote_node_list):
        """
        Merges the given remote_node_list and the internal node_list,
        setting each timestamp to the newer one.
        :param remote_node_list: A python dictionary as returned by from_json
        :type remote_node_list: dict
        :return: List of triples (node_id, host, port) of new peers
        :rtype: List[(node id, host, port)]
        """
        # Skip if they are equal already
        if remote_node_list == self.node_list:
            return []

        merged_dict = self.node_list
        new_peers = []

        for node_id, node in remote_node_list.items():
            # Skip myself
            if node_id == self.my_id:
                # continue
                pass

            # Check if node_id is already contained or not
            if node_id in merged_dict:
                merged_node = merged_dict[node_id]
                time_stamp = node["timestamp"]
                if merged_node["timestamp"] is None:
                    merged_node["timestamp"] = time_stamp  # update timestamp as it is None
                    continue
                if time_stamp > merged_node["timestamp"]:
                    merged_node["timestamp"] = time_stamp  # update timestamp as remote's timestamp is newer

            else:
                # Add new peer
                merged_dict[node_id] = node
                host = node["host"]
                port = node["port"]
                new_peers.append((node_id, host, port))  # split into tuple (node_id, host,port)

        self.node_list = merged_dict
        logger.debug(self.to_string())

        if new_peers:
            self.store()

        return new_peers

    def equals(self, other):
        """
        Check if two NodeList objects are equal. If the other object is not of class NodeList,
        then they are not equal. Two NodeLists are equal if their two node lists are equal.
        :param other: NodeList object to compare to
        :type other: object
        :return: True if they are equal, False otherwise.
        :rtype: bool
        """
        if not isinstance(other, type(self)):
            return False
        return self.node_list == other.node_list

    def to_string(self):
        """
        Creates a human readable string representation of the node list
        :return: String representation of the current node list
        :rtype: str
        """
        out = "Node id, Address, Port, Latest Connection\n"
        for node_id, host, port in self.iterate():
            time_stamp = self.last_successful_connection_unix(node_id)
            date = datetime.datetime.fromtimestamp(time_stamp)
            readable = date.strftime('%Y-%m-%d %H:%M:%S')
            out += f"{node_id}, {host}, {port}, {readable}\n"
        return out

    def store(self):
        """
        Saves the current nodelist to the set path.
        If no path is set, does nothing.
        :return: No return
        """
        if self.path:
            json_list = self.repr_json()
            with open(self.path, 'w+') as file:
                file.write(json_list)
            logger.debug(f"Saved node list to {self.path}")
        else:
            logger.debug(f"NodeList not saved, no path set.")

    def load(self):
        """
        Loads a nodelist from the given filepath and merges it into the current nodelist.
        :return: No return
        """
        if os.path.exists(self.path) and os.path.isfile(self.path):
            with open(self.path, 'r') as file:
                loaded_nodelist = from_json(file.readline())
            self.merge(loaded_nodelist)
            logger.debug("Loaded nodelist from file.")
        else:
            with open(self.path, 'w+') as file:
                pass
            logger.debug("Nodelist file not found, creating file.")

    def is_empty(self):
        """
        Returns true if this node list is empty.
        :return: True if empty, otherwise false.
        :rtype: bool
        """
        return len(self.node_list.keys()) == 0
